<?php

function run_user_trade_individual_product_price( $post_ID, $post_data ){
	
	$get_post_type = get_post_type($post_ID);

	if( 'tradeuser_price_tier' == $get_post_type ){

		$get_trade_user 			= get_field('user_id',$post_ID);
		$get_trade_user_product		= get_field('tier_pricing',$post_ID);
		$array_product_update		= array();

		if( $get_trade_user_product ){
			
			foreach( $get_trade_user_product as $product ){
				$get_product_id 		= $product['product_name']; ##return kay id
				$stored_trade_user		= get_field('trade_user',$get_product_id);
			  	$array_product_update[]	= $get_trade_user;

				if( empty($stored_trade_user) ){
					update_field('trade_user',$array_product_update, $get_product_id);
				}else{
					$stored_data 	= get_field('trade_user',$get_product_id);
					$merged_array 	= array_merge($stored_data,$array_product_update);
					update_field('trade_user',$merged_array,$get_product_id);
				}

			}

		}

	}

}
add_action('pre_post_update','run_user_trade_individual_product_price');
add_action('save_post_tradeuser_price_tier','run_user_trade_individual_product_price');


function trade_user_per_product_price( $userid ){
	
	// ob_start();
		set_query_var( 'get_user_id_trade', $userid->ID );

		get_template_part('includes/admin/content','trade_user_product_prices'	);
		// $content = ob_get_contents();
    // ob_end_clean();

}
add_action('show_user_profile', 'trade_user_per_product_price');
add_action('edit_user_profile', 'trade_user_per_product_price');


function update_trade_user_per_product_price( $userid ){
		
		$parent_category_keys 	= array();
		$sub_category_keys		= array();

		$taxonomy     = 'product_cat';
		$orderby      = 'name';  
		$show_count   = 0;      // 1 for yes, 0 for no
		$pad_counts   = 0;      // 1 for yes, 0 for no
		$hierarchical = 1;      // 1 for yes, 0 for no  
		$title        = '';  
		$empty        = 0;

		$args = array(
		     'taxonomy'     => $taxonomy,
		     'orderby'      => $orderby,
		     'show_count'   => $show_count,
		     'pad_counts'   => $pad_counts,
		     'hierarchical' => $hierarchical,
		     'title_li'     => $title,
		     'hide_empty'   => $empty
		);

		$all_categories = get_categories( $args );

		if( $all_categories ){
			
			$parent_counter = 0;
			$child_counter	= 0;

			foreach ( $all_categories as $cat ) {
				
				if( $cat->category_parent == 0 ){

					$category_id = $cat->term_id;	

					$option_value = '_' . $cat->slug . '_trade_user' ; 

					$register_meta = add_user_meta( $userid, $option_value, $_POST[$option_value] , true );

					if( ! $register_meta ){
						update_user_meta($userid, $option_value , $_POST[$option_value] );
					}

					$parent_category_keys[$parent_counter][$option_value] = get_user_meta($userid,$option_value);

					$args2 = array(
			                'taxonomy'     => $taxonomy,
			                'child_of'     => 0,
			                'parent'       => $category_id,
			                'orderby'      => $orderby,
			                'show_count'   => $show_count,
			                'pad_counts'   => $pad_counts,
			                'hierarchical' => $hierarchical,
			                'title_li'     => $title,
			                'hide_empty'   => $empty
			        );

			        $sub_cats = get_categories( $args2 );

			        if($sub_cats) {
			        	
			            foreach($sub_cats as $sub_category) {
							
							$option_value_child = '_' . $sub_category->slug . '_trade_user'; 

							$register_meta = add_user_meta( $userid, $option_value_child, $_POST[$option_value_child] , true );

							if( !$register_meta )
								update_user_meta($userid, $option_value_child , $_POST[$option_value_child] );

							$sub_category_keys[$child_counter][$option_value_child] = get_user_meta($userid,$option_value_child);
							
							$child_counter++;
						}

					}
					$parent_counter++;
				}
			}
		}

		// echo '<pre>';
		// 	print_r($parent_category_keys);
		// echo '</pre>';

		// echo '<pre>';
		// 	print_r($sub_category_keys);
		// echo '</pre>';

		// die();
}

add_action('personal_options_update', 'update_trade_user_per_product_price');
add_action('edit_user_profile_update', 'update_trade_user_per_product_price');