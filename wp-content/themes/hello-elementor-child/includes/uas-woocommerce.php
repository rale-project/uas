<?php

// WooCommerce Hooks and Customizations


function change_lost_your_password ($text) {

    if ($text == 'Lost your password?'){
        $text = 'forgot your password?';
    }
    return $text;

}
add_filter( 'gettext', 'change_lost_your_password' );

function wc_custom_lost_password_form( $atts ) {

    return wc_get_template( 'myaccount/form-lost-password.php', array( 'form' => 'lost_password' ) );

}
add_shortcode( 'lost_password_form', 'wc_custom_lost_password_form' );

add_action('wp_head', 'uas_ajaxurl');

function uas_ajaxurl() {

   echo '<script type="text/javascript">
           var ajaxurl = "' . admin_url('admin-ajax.php') . '";
         </script>';
}

add_action('woocommerce_shop_loop_item_title','sku_product_category_display',11);

function sku_product_category_display(){
    global $product;

    if( wc_get_product_category_list($product->get_id()) && $product->get_sku() ){
        echo '<div class="sku__and_cat">' . wc_get_product_category_list($product->get_id()) . ' | ' . $product->get_sku() . '</div>';
    }

    if( wc_get_product_category_list($product->get_id()) && $product->get_sku() == '' ){
        echo '<div class="sku__and_cat">' . wc_get_product_category_list($product->get_id()) . '</div>';
    }
     
    if( wc_get_product_category_list($product->get_id()) == '' && $product->get_sku() ){
        echo '<div class="sku__and_cat">' . $product->get_sku() . '</div>';
    }

}

add_filter( 'woocommerce_get_availability', 'wcs_custom_get_availability', 1, 2);
function wcs_custom_get_availability( $availability, $_product ) {
    
    // Change In Stock Text
    if ( $_product->is_in_stock() ) {
        $availability['availability'] = __('Available!', 'woocommerce');
    }
    // Change Out of Stock Text
    if ( ! $_product->is_in_stock() ) {
        $availability['availability'] = __('Sold Out', 'woocommerce');
    }
    return $availability;
}

function pm_wc_format_stock_for_display(  ) {

    global $product;

    $display      = __( 'In stock', 'woocommerce' );
    $stock_amount = $product->get_stock_quantity();

    switch ( get_option( 'woocommerce_stock_format' ) ) {
        case 'low_amount':
            if ( $stock_amount <= get_option( 'woocommerce_notify_low_stock_amount' ) ) {
                /* translators: %s: stock amount */
                $display = sprintf( __( 'Only %s left in stock', 'woocommerce' ), wc_format_stock_quantity_for_display( $stock_amount, $product ) );
            }
            break;
        case '':
            /* translators: %s: stock amount */
            $display = sprintf( __( 'Stock Avialable %s', 'woocommerce' ), wc_format_stock_quantity_for_display( $stock_amount, $product ) );
            break;
    }

    if ( $product->backorders_allowed() && $product->backorders_require_notification() ) {
        $display .= ' ' . __( '(can be backordered)', 'woocommerce' );
    }

    return $display;
}


/**
 *  This is the hooks of elementor form after form submit **** FOR REGULAR USER ONLY! ****
 */
add_action( 'elementor_pro/forms/new_record',  'paul_elementor_form_create_new_user_regular' , 10, 2 );

function paul_elementor_form_create_new_user_regular($record,$ajax_handler) // creating function 
{
    $form_name = $record->get_form_settings('form_name');
    
    //Check that the form is the "Sign Up" if not - stop and return;
    if ('Registration' !== $form_name) {
        return;
    }
    
    $form_data  = $record->get_formatted_data();
    
    //Get the value of the input with the label  
    $username   =   $form_data['Username'];
    $password   =   $form_data['Password'];
    $email      =   $form_data['Email Address'];

    // Billing Details
    $fname      =   $form_data['First Name'];
    $lname      =   $form_data['Last Name'];
    $pnumber    =   $form_data['Contact Number'];
    $address    =   $form_data['Address'];
    $city       =   $form_data['City/Town'];
    $state      =   $form_data['State'];
    $postcode   =   $form_data['Postcode'];
    $country    =   $form_data['Country'];
    
    // This is the required field to create a user in wordpress. Let create user with wordpress function wp_create_user();
        //wp_create_user( string $username, string $password, string $email = '' )
    $user = wp_create_user($username,$password,$email); // Create a new user, on success return the user_id no failure return an error object

    if (is_wp_error($user)){ // if there was an error creating a new user
        $ajax_handler->add_error_message("Failed to create new user: ".$user->get_error_message()); //add the message
        $ajax_handler->is_success = false;
        return;
    }

  // Next, We are going to add user related information which is called user meta data with the function wp_update_user
    wp_update_user(
        array(
            "ID"                    =>  $user,
            "first_name"            =>  $fname,
            "last_name"             =>  $lname,
        )
    ); 
    update_field('billing_country',$country,  'user_' . $user);
    update_field('billing_state',$state,  'user_' . $user);
    update_field('billing_address_1',$address,  'user_' . $user);
    update_field('billing_city',$city,'user_' . $user);
    update_field('billing_postcode',$postcode,'user_' . $user);
    update_field('billing_phone',$pnumber,'user_' . $user);
            
    /* Automatically log in the user and redirect the user to the home page */
    $creds= array( // credientials for newley created user
        "user_login"=>$username,
        "user_password"=>$password,
        "remember"=>true
    );
    
    $signon = wp_signon($creds); // sign in the new user
    if ($signon)
      
    // Set redirect action
      $ajax_handler->add_response_data( 'redirect_url', get_home_url() . '/dashboard/' ); // optinal - if sign on succsfully - redierct the user to the home page
}

/**
 *  This is the hooks of elementor form after form submit **** FOR WHOLESALER USER ONLY! ****
 */
add_action( 'elementor_pro/forms/new_record',  'paul_elementor_form_create_new_user_wholesaler' , 10, 2 );

function paul_elementor_form_create_new_user_wholesaler($record,$ajax_handler) // creating function 
{
    $form_name = $record->get_form_settings('form_name');
    
    //Check that the form is the "Sign Up" if not - stop and return;
    if ('Trade Account' !== $form_name) {
        return;
    }
    
    $form_data  = $record->get_formatted_data();
    
    //Get the value of the input with the label  
    $username   =   $form_data['Username'];
    $password   =   $form_data['Password'];
    $email      =   $form_data['Email Address'];

    // Billing Details
    $fname                 =   $form_data['First Name'];
    $lname                 =   $form_data['Last Name'];
    $pnumber               =   $form_data['Contact Number'];
    $company               =   $form_data['Company Name'];
    $abnCapricronNumber    =   $form_data['ABN / Capricorn number'];
    $address               =   $form_data['Address'];
    $cityTown              =   $form_data['City/Town'];
    $state                 =   $form_data['State'];
    $postCode              =   $form_data['Post Code'];
    $userUrl               =   $form_data['User Link'];
    
    $user = wp_create_user($username,$password,$email); // Create a new user, on success return the user_id no failure return an error object

    if (is_wp_error($user)){ // if there was an error creating a new user
        $ajax_handler->add_error_message("Failed to create new user: ".$user->get_error_message()); //add the message
        $ajax_handler->is_success = false;
        return;
    }

  // Next, We are going to add user related information which is called user meta data with the function wp_update_user
    wp_update_user(
        array(
            "ID"                    =>  $user,
            "first_name"            =>  $fname,
            "last_name"             =>  $lname,
        )
    ); 

    // Update the user role to Wholesaler
    $wholesaler = new WP_User( $user );

    $wholesaler->remove_role('customer');

    $wholesaler->add_role('whole_saler');

    update_field( 'billing_company', $company , 'user_' . $user );
    update_field( 'billing_address_1', $address, 'user_' . $user);
    update_field( 'abn__capricorn_number', $abnCapricronNumber , 'user_' . $user );
    update_field( 'billing_phone', $pnumber, 'user_' . $user );

    update_field( 'billing_city', $cityTown, 'user_' . $user );
    update_field( 'billing_postcode', $postCode, 'user_' . $user );
    update_field( 'billing_state', $state, 'user_' . $user );       
    

    /* Automatically log in the user and redirect the user to the home page */
    // $creds= array( // credientials for newley created user
    //     "user_login"=>$username,
    //     "user_password"=>$password,
    //     "remember"=>true
    // );
    
    // $signon = wp_signon($creds); // sign in the new user
    // if ($signon)
      
    //   // Set redirect action
    //   $ajax_handler->add_response_data( 'redirect_url', get_home_url() . '/dashboard' ); // optinal - if sign on succsfully - redierct the user to the home page
}

add_action( 'elementor_pro/forms/new_record',  'paul_elementor_form_request_a_quote' , 10, 4 );

function paul_elementor_form_request_a_quote($record,$ajax_handler) // creating function 
{
    $form_name = $record->get_form_settings('form_name');
    
    //Check that the form is "Sign Up" if not - stop and return;
    if ('Request a quote' !== $form_name) {
        return;
    }
    
    $form_data  = $record->get_formatted_data();
    
    //Get the value of the input with the label  
    $email      =   $form_data['Email Address'];

    // Billing Details
    $fname                 =   $form_data['First Name'];
    $lname                 =   $form_data['Last Name'];
    $pnumber               =   $form_data['Contact Number'];
    $company               =   $form_data['Company Name'];
    $abnCapricronNumber    =   $form_data['ABN'];
    $address               =   $form_data['Address'];
    $notes                 =   $form_data['Notes regarding your order..'];
    // $requestId             =   $form_data['Request ID'];

    $cart       = WC()->cart;
    $checkout   = WC()->checkout();
    $order_id   = $checkout->create_order($cart->cart_contents);
    $order      = new WC_Order( $order_id );

    // The text for the note
    $note = __("ABN : " . $abnCapricronNumber .' Note : ' . $notes);
   
    // Add the note
    $order->add_order_note( $note );

    update_field( '_billing_first_name', $fname, $order_id);
    update_field( '_billing_last_name', $lname, $order_id);
    update_field( '_billing_company', $company , $order_id );
    update_field( '_billing_address_1', $address, $order_id);
    update_field( '_billing_phone', $pnumber , $order_id );
    update_field( '_billing_email', $email, $order_id );
            
   
}


add_action('wp_ajax_save_item_cart','save_cart_items_as_order');
add_action('wp_ajax_nopriv_save_item_cart','save_cart_items_as_order');
function save_cart_items_as_order(){

    global $woocommerce;

    if( !is_user_logged_in() ){
        return false;
    }

    $localStorage_orderId =  (int) $_POST['order_number'];
    $current_user         =   sanitize_text_field($_POST['current_user']);


    $cart = WC()->cart;
    $checkout = WC()->checkout();
    $result = [];
    
    if( WC()->cart->get_cart_contents_count() == 0 ){
        return;
    }
    
    if( $localStorage_orderId ){
        wp_delete_post($localStorage_orderId,true);
    }

    $order_id = $checkout->create_order($cart->cart_contents);
    $order = wc_get_order( $order_id );
    $result['ordernumber'] = $order_id;
    $result['orderlink'] = get_edit_post_link( $order_id );
    

    $new_order = new WC_Order($order_id);

    if( wc_current_user_has_role('whole_saler') ){
        $new_order->update_status('request-quote'); 
        $result['dashboard_link'] = site_url() . '/dashboard/quote-details/';

        $end_date = date('m/d/Y H:m:s', strtotime("+30 days"));

        update_field('date_expiry',$end_date,$order_id);

    }

    if( wc_current_user_has_role('customer') ){
        $new_order->update_status('pending');
        $result['dashboard_link'] = site_url() . '/dashboard/order-details/';
    }

    WC()->cart->empty_cart();

    wp_send_json_success( $result );

    die();
}

function add_request_a_quote_to_order_statuses( $order_statuses ) {
 
    $new_order_statuses = array();
 
    // add new order status after processing
    foreach ( $order_statuses as $key => $status ) {
 
        $new_order_statuses[ $key ] = $status;
 
        if ( 'wc-pending' === $key ) {
            $new_order_statuses['wc-request-quote'] = 'Request a Quote';
        }
    }
 
    return $new_order_statuses;
}
add_filter( 'wc_order_statuses', 'add_request_a_quote_to_order_statuses' );

function register_request_quote_order_status() {
    register_post_status( 'wc-request-quote', array(
        'label'                     => 'Request a Quote',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Request a Quote (%s)', 'Request a Quote (%s)' )
    ) );
}
add_action( 'init', 'register_request_quote_order_status' );


add_action('woocommerce_after_shop_loop_item','add_custom_trade_button',15 );
function add_custom_trade_button(){ 

    if( !wc_current_user_has_role('whole_saler') ){
        echo '<a href="'. site_url() .'/trade-account-register/" class="ctm_trade__price">Trade Price</a>';
    }

}

add_action('woocommerce_before_shop_loop_item','check_if_product_is_out_of_stock');

function check_if_product_is_out_of_stock(){

    global $product;
    //Check if lower than treshold || $product->is_in_stock()
    $treshold   = (int) get_field('_low_stock_amount',$product->get_ID());
    $stock      = (int) get_field('_stock',$product->get_ID());

    if ( $product->managing_stock() && $stock <= $treshold )
        echo '<span class="out-of-stock ctm-stock onsale">Restocked</span>';

}

add_filter('woocommerce_sale_flash', 'woocommerce_custom_sale_text', 10, 3);
function woocommerce_custom_sale_text($text, $post, $_product)
{

    $treshold   = (int) get_field('_low_stock_amount',$_product->get_ID());
    $stock      = (int) get_field('_stock',$_product->get_ID());

    if ( $_product->managing_stock() && $stock <= $treshold ){
        return;
    }else{
        return '<span class="onsale">On Sale</span>';
    }
    
}

// Display Fields
add_action('woocommerce_product_options_general_product_data', 'woocommerce_product_trade_price_custom_fields');
// Save Fields
add_action('woocommerce_process_product_meta', 'woocommerce_product_trade_price_custom_fields');
function woocommerce_product_trade_price_custom_fields()
{
    global $woocommerce, $post;

    echo '<div class="product_custom_field">';
    // Custom Product Text Field
    woocommerce_wp_text_input(
        array(
            'id' => 'price_list_1',
            'placeholder' => '',
            'label' => __('Price List 1', 'woocommerce'),
            // 'desc_tip' => 'true'
        )
    );

    woocommerce_wp_text_input(
        array(
            'id' => 'price_list_2',
            'placeholder' => '',
            'label' => __('Price List 2', 'woocommerce'),
        )
    );

    woocommerce_wp_text_input(
        array(
            'id' => 'price_list_3',
            'placeholder' => '',
            'label' => __('Price List 3', 'woocommerce'),
        )
    );

    woocommerce_wp_text_input(
        array(
            'id' => 'price_list_4',
            'placeholder' => '',
            'label' => __('Price List 4', 'woocommerce'),
        )
    );
    
    woocommerce_wp_text_input(
        array(
            'id' => 'price_ebay',
            'placeholder' => '',
            'label' => __('Price Ebay', 'woocommerce'),
            // 'desc_tip' => 'true'
        )
    );

    woocommerce_wp_text_input(
        array(
            'id' => 'price_2',
            'placeholder' => '',
            'label' => __('Price 2', 'woocommerce'),
            // 'desc_tip' => 'true'
        )
    );

    woocommerce_wp_text_input(
        array(
            'id' => 'price_3',
            'placeholder' => '',
            'label' => __('Price 3', 'woocommerce'),
            // 'desc_tip' => 'true'
        )
    );

    woocommerce_wp_text_input(
        array(
            'id' => 'price_4',
            'placeholder' => '',
            'label' => __('Price 4', 'woocommerce'),
            // 'desc_tip' => 'true'
        )
    );

    woocommerce_wp_text_input(
        array(
            'id' => 'price_reseller',
            'placeholder' => '',
            'label' => __('Price reseller', 'woocommerce'),
            // 'desc_tip' => 'true'
        )
    );

    woocommerce_wp_text_input(
        array(
            'id' => 'cost_price',
            'placeholder' => '',
            'label' => __('Cost Price', 'woocommerce'),
            // 'desc_tip' => 'true'
        )
    );

    echo '</div>';
}


add_action('woocommerce_process_product_meta','woocommerce_trade_product_price_save_data');
function woocommerce_trade_product_price_save_data($post_id)
{

    $price_list_1 = $_POST['price_list_1'];
    if (!empty($price_list_1))
        update_post_meta($post_id, 'price_list_1', esc_attr($price_list_1));

    $price_list_2 = $_POST['price_list_2'];
    if (!empty($price_list_2))
        update_post_meta($post_id, 'price_list_2', esc_attr($price_list_2));

    $price_list_3 = $_POST['price_list_3'];
    if (!empty($price_list_3))
        update_post_meta($post_id, 'price_list_3', esc_attr($price_list_3));

    $price_list_4 = $_POST['price_list_4'];
    if (!empty($price_list_4))
        update_post_meta($post_id, 'price_list_4', esc_attr($price_list_4));

    $price_ebay = $_POST['price_ebay'];
    if (!empty($price_ebay))
        update_post_meta($post_id, 'price_ebay', esc_attr($price_ebay));

    $price_2 = $_POST['price_2'];
    if (!empty($price_2))
        update_post_meta($post_id, 'price_2', esc_attr($price_2));

    $price_3 = $_POST['price_3'];
    if (!empty($price_3))
        update_post_meta($post_id, 'price_3', esc_attr($price_3));

    $price_4 = $_POST['price_4'];
    if (!empty($price_4))
        update_post_meta($post_id, 'price_4', esc_attr($price_4));

    $price_reseller = $_POST['price_reseller'];
    if (!empty($price_reseller))
        update_post_meta($post_id, 'price_reseller', esc_attr($price_reseller));

    $cost_price = $_POST['cost_price'];
    if (!empty($cost_price))
        update_post_meta($post_id, 'cost_price', esc_attr($cost_price));

}

 
add_filter( 'woocommerce_get_price_html', 'trade_alter_price_display', 9999, 2 );
 
function trade_alter_price_display( $price_html, $product ) {
    
    $get_terms = get_the_terms( $product->get_id(), 'product_cat' );

    // ONLY ON FRONTEND
    if ( is_admin() ) return $price_html;
    
    
    if ( wc_current_user_has_role( 'whole_saler' ) ) {

        if( $get_terms ){

            // pull the 0 index term_id 
            $term_id    = $get_terms[0]->term_id;
            $slug       = $get_terms[0]->slug;
            // pull the price tier for current user
            $user_id    = get_current_user_id();
            $price_tier = get_user_meta( $user_id, '_' . $slug . '_trade_user', true );

            if( $price_tier ){
                $price_tier = str_replace("-","_",$price_tier);    
                $price_tier = get_post_meta( $product->get_id(), $price_tier, true );
            }

            if( ! $price_tier || $price_tier == ' ' || $price_tier == '' ){

                foreach ( $get_terms as $term ) {
                    
                    $term_id = $term->term_id;
                    $parent  = $term->parent;

                    if( $parent == 0 ){
                        $price_tier = get_user_meta( $user_id, '_' . $term->slug . '_trade_user', true );

                        if( $price_tier ){
                            $price_tier = str_replace("-","_",$price_tier);
                            $price_tier = get_post_meta( $product->get_id(), $price_tier , true );
                        }

                        if( !$price_tier ){
                            $price_tier = get_post_meta( $product->get_id(), 'price_list_1' , true );
                        }

                        break;
                    }

                }

                if( ! $price_tier ){
                    $price_tier = get_post_meta( $product->get_id(), 'price_list_1' , true );
                }

            }
            
            $price_html = '<p class="price">'. wc_price( $price_tier ) .'</p>';

        }

    }
    
    return $price_html;
 
}


add_action( 'woocommerce_before_calculate_totals', 'paul_alter_price_cart', 9999 );
 
function paul_alter_price_cart( $cart ) {
 
    if ( is_admin() && ! defined( 'DOING_AJAX' ) ) return;

    if( wc_current_user_has_role('whole_saler') ){

        foreach ( $cart->get_cart() as $cart_item_key => $cart_item ) {

            $product = $cart_item['data'];

            $get_terms = get_the_terms( $product->get_id(), 'product_cat' );

            if( $get_terms ){

                // pull the 0 index term_id 
                $term_id    = $get_terms[0]->term_id;
                $slug       = $get_terms[0]->slug;
                // pull the price tier for current user
                $user_id    = get_current_user_id();
                $price_tier = get_user_meta( $user_id, '_' . $slug . '_trade_user', true );

                if( $price_tier ){
                    $price_tier = str_replace("-","_",$price_tier);    
                    $price_tier = get_post_meta( $product->get_id(), $price_tier, true );
                }

                if( ! $price_tier || $price_tier == ' ' || $price_tier == '' ){

                    foreach ( $get_terms as $term ) {
                        
                        $term_id = $term->term_id;
                        $parent  = $term->parent;

                        if( $parent == 0 ){
                            $price_tier = get_user_meta( $user_id, '_' . $term->slug . '_trade_user', true );

                            if( $price_tier ){
                                $price_tier = str_replace("-","_",$price_tier);
                                $price_tier = get_post_meta( $product->get_id(), $price_tier , true );
                            }

                            if( !$price_tier ){
                                $price_tier = get_post_meta( $product->get_id(), 'price_list_1' , true );
                            }

                            break;
                        }

                    }

                    if( ! $price_tier ){
                        $price_tier = get_post_meta( $product->get_id(), 'price_list_1' , true );
                    }

                }
                
            }

            $cart_item['data']->set_price( $price_tier );
        }

    }
 
}

add_action( 'template_redirect', 'wc_redirect_to_shop');
function wc_redirect_to_shop() {
    // Only on product category archive pages (redirect to shop)
    if ( is_product_category() ) {
        wp_redirect( site_url( 'products' ) );
        exit();
    }

    if ( wc_current_user_has_role('customer') && is_page(734) ){
        wp_redirect( site_url() );
        exit();
    }
}

add_action( 'woocommerce_order_status_pending', 'pending_new_order_notification', 20, 1 );
function pending_new_order_notification( $order_id ) {

    $order = wc_get_order( $order_id );

    if( ! $order->has_status( 'pending' ) ) return;

    WC()->mailer()->get_emails()['WC_Email_New_Order']->trigger( $order_id );

}

add_action( 'woocommerce_order_status_processing', 'send_admin_email_order_processing', 20, 2 );

function send_admin_email_order_processing( $order_id ){

    $order = wc_get_order( $order_id );
    ob_start();
        wc_get_template( 'emails/customer-processing-order.php', array(
            'order'         => $order,
            'email_heading' => 'Please Process this Request',
            'sent_to_admin' => true,
            'email'         => '',
            'plain_text'    => false
        ) );
    $content = ob_get_contents();
    ob_end_clean();

    $wc_email_receiver = get_option('admin_email');

    $to = $wc_email_receiver;
    $subject = 'UAS - Order needs to be processes #' . $order_id;
    // the domain from must be replace during LIVE.
    // $headers[] = 'From: United Automotive Solutions<hello@auwebstaging.com>';
    $headers[] = 'Content-Type: text/html; charset=UTF-8';

    wp_mail($to,$subject,$content,$headers);

}

function save_quote_cart_page( $woocommerce_button_proceed_to_checkout, $int ) { 
    // make action magic happen here...
 
    if( wc_current_user_has_role('whole_saler') ){
        echo '<a href="#" class="checkout-button button alt wc-forward">
        Save Quote</a>';
    }else{
        echo '<a href="'. esc_url( wc_get_checkout_url() ) .'" class="checkout-button button alt wc-forward">
        Place Order</a>';
    }
} 
         
remove_action( 'woocommerce_proceed_to_checkout', 'woocommerce_button_proceed_to_checkout', 20); 
add_action( 'woocommerce_proceed_to_checkout', 'save_quote_cart_page', 10, 2 );


add_action('wp_ajax_save_edited_cart','update_order_based_cart_edit_items');
add_action('wp_ajax_nopriv_save_edited_cart','update_order_based_cart_edit_items');
function update_order_based_cart_edit_items(){

    if( !wc_current_user_has_role('whole_saler') ){
        return false;
    }

     $order_id = absint($_POST['order_number']);

     $obj_order = new WC_Order($order_id);
     $cart = WC()->cart;

    if( $order_id ){
        wp_delete_post($order_id,true);
    }
     
    $order_id = WC()->checkout()->create_order($cart->cart_contents);
    $order = wc_get_order( $order_id );
    $result['ordernumber'] = $order_id;
    $result['orderlink'] = get_edit_post_link( $order_id );
    $result['dashboard_link'] = site_url() . '/dashboard/quote-details/';

    $new_order = new WC_Order($order_id);
    $new_order->update_status('request-quote','order_note');

    WC()->cart->empty_cart();

    wp_send_json_success( $result );

    die();

}

add_action('wp_ajax_edit_items_to_cart','place_back_order_item_to_cart');
add_action('wp_ajax_nopriv_edit_items_to_cart','place_back_order_item_to_cart');
function place_back_order_item_to_cart(){

    global $woocommerce;

    $order_id = (int) $_POST['order_id'];

    $woocommerce->cart->empty_cart();

    $get_order = wc_get_order( $order_id );

    if( $get_order->get_items() ){
        foreach ($get_order->get_items() as $item_id => $item) {

            $product_id = $item->get_product_id();
            $variation_id = $item->get_variation_id();
            $quantity = $item->get_quantity();

            if( !$variation_id ){
                WC()->cart->add_to_cart( $product_id, $quantity );    
            }

            if( $variation_id ){
                WC()->cart->add_to_cart( $product_id, $quantity, $variation_id );
            }

        }
    }

    $cart_url['cart_url'] = wc_get_cart_url();

    wp_send_json_success( $cart_url );

    die();

}

function generate_pdf_request_quote_order_actions( $actions, $order ) {

    $url_pdf_generator  = site_url() . '/dashboard/order-details/?pdfgenerator=' . $order->get_order_number();
    $edit_quote         = '#';
    $date_expiry        = strtotime( get_field('date_expiry',$order->get_order_number() ) );
    $current_date       = strtotime( date('m/d/Y H:m:s') ); 
    
    $actions['download_quote'] = array(
        'url'  => $url_pdf_generator,
        'name' => 'Download',
    );

    if( is_page(734) && wc_current_user_has_role('whole_saler') ){

        $actions['edit_cart_items'] = array(
            'url'   => $edit_quote,
            'name'  => 'Edit Items'
        );

    }

    if( is_page(734) && wc_current_user_has_role('whole_saler') ){
        if( $current_date >= $date_expiry ){
           unset( $actions['download_quote'] );   
           unset( $actions['edit_cart_items'] );
        }
    }

    return $actions;

}
add_filter( 'woocommerce_my_account_my_orders_actions', 'generate_pdf_request_quote_order_actions', 10, 2 ); 

add_filter('woocommerce_my_account_my_orders_actions', 'remove_my_cancel_button', 10, 2);
function remove_my_cancel_button($actions, $order){
    unset($actions['cancel']);
    return $actions;
}

function action_woocommerce_view_order( $order_id ) {

    $order = wc_get_order( $order_id );
    $order_status  = 'wc-'.$order->get_status();    
    
    $current_user = wp_get_current_user();
    
    

        if( $current_user->roles[0] == 'whole_saler' && $order_status == 'wc-request-quote' ){

        $order_statuses = array( 
                'wc-request-quote' => _x( 'Request a Quote', 'Order status', 'woocommerce' ),  
                'wc-processing' => _x( 'Process this Order', 'Order status', 'woocommerce' ),  
                // 'wc-on-hold' => _x( 'On hold', 'Order status', 'woocommerce' ),  
                // 'wc-cancelled' => _x( 'Cancelled', 'Order status', 'woocommerce' ),  
         );
    ?>
            <form class="woocommerce-form update_order_status" method="post">
                <h2>Quote Status</h2>
                <select name="select-updated-status">
                    <?php 
                        foreach( $order_statuses as $status => $val ){
                    ?>
                        <option value="<?php echo $status; ?>" <?php echo $is_selected = ( $order_status == $status ) ? 'selected' : ''; ?> ><?php echo $val; ?></option>
                    <?php
                        }
                    ?>
                </select>
                <!-- <h3>Order Notes</h3> -->

                <?php //wp_nonce_field( 'woocommerce-update-order-status', 'woocommerce-update-order-status-nonce' ); ?>
                <input type="hidden" name="the_order_id" value="<?php echo $order_id; ?>">
                <button type="submit" class="woocommerce-button button" name="orderUpdateStatus" value="<?php esc_attr_e( 'Update Status', 'woocommerce' ); ?>"><?php esc_html_e( 'Update Status', 'woocommerce' ); ?></button>
            </form>
    <?php
        }

}
add_action( 'woocommerce_view_order', 'action_woocommerce_view_order', 2, 2 );


add_filter( 'woocommerce_product_data_tabs', 'add_trade_user_product_data_tab', 99 , 1 );
function add_trade_user_product_data_tab( $product_data_tabs ) {
    $product_data_tabs['trade-user'] = array(
        'label' => __( 'Trade Users', 'uas_site' ), // translatable
        'target' => 'trade_user_product_data_tab', // translatable
    );
    return $product_data_tabs;
}

add_filter( 'woocommerce_my_account_my_orders_columns', 'display_column_orders_quote_page' );
function display_column_orders_quote_page( $columns = array() ){
    
    if( !is_page(734) ){
        unset( $columns['date-expiry'] );    
    }

    return $columns;

}

// add_action( 'woocommerce_product_data_panels', 'add_trade_user_product_data_fields' );
function add_trade_user_product_data_fields() {
    
    global $post;

    $post_id = $post->ID;

    $args = array(
        'post_type'     => 'tradeuser_price_tier',
        'numberposts'   => -1,
    );


    $tradeuser_posts = get_posts($args);
    

    echo '<div id="trade_user_product_data_tab" class="panel woocommerce_options_panel">';
    ?>

        <table>
            <thead>
                <tr>
                    <th>
                        #
                    </th>
                    <th>
                        Trade Name
                    </th>
                    <th>
                        Trade Price
                    </th>
                    <th>
                        Sale Trade Price
                    </th>
                </tr>
            </thead>
            <tbody>
            <?php 
                if( $tradeuser_posts ){
                    $counter = 1;
                    foreach ( $tradeuser_posts as $user ) { 
                        $match_product  = get_field('tier_pricing',$user->ID);
                        foreach( $match_product as $product ){

                            if( $product['product_name'] == $post_id ){
                                $user_id        = get_field( 'user_id',$user->ID ); 
                                $user_url       = '/post.php?post=' . $user->ID . '&action=edit';
                                $display_name   = $user->post_title;
            ?>
                                 <tr>
                                     <td>
                                         <?php echo $counter; ?>
                                     </td>
                                     <td>
                                     <a href="<?php echo admin_url($user_url); ?>"><?php echo $display_name; ?></a>    
                                     </td>
                                </tr>
            <?php               $counter++;
                            }
                        }

                    
                    }
                }
            ?>
            </tbody>
        </table>

    <?php 
    echo '</div>';
}

#products per page
add_action( 'woocommerce_before_shop_loop', 'ps_selectbox', 25 );
function ps_selectbox() {
    $per_page = filter_input(INPUT_GET, 'perpage', FILTER_SANITIZE_NUMBER_INT);     
    echo '<div class="woocommerce-perpage">';
    echo '<select onchange="if (this.value) window.location.href=this.value">';   
    $orderby_options = array(
        '-1' => 'All',
        '50' => '50',
        '250' => '250',
        '500' => '500'
    );
    foreach( $orderby_options as $value => $label ) {
        echo "<option ".selected( $per_page, $value )." value='?perpage=$value'>$label</option>";
    }
    echo '</select>';
    echo '</div>';
}
add_action( 'pre_get_posts', 'ps_pre_get_products_query' );
function ps_pre_get_products_query( $query ) {
   $per_page = filter_input(INPUT_GET, 'perpage', FILTER_SANITIZE_NUMBER_INT);
   if( $query->is_main_query() && !is_admin() && is_post_type_archive( 'product' ) ){
        $query->set( 'posts_per_page', $per_page );
    }
}
#products per page end

#stock quantity
function show_stock() {
global $product;
if ( $product->get_stock_quantity() ) { // if manage stock is enabled 
if ( number_format($product->get_stock_quantity(),0,'','') < 3 ) { // if stock is low
echo '<div class="remaining" style="padding:10px;">Only ' . number_format($product->get_stock_quantity(),0,'','') . ' in stock!</div>';
} else {
echo '<div class="remaining" style="padding:10px;">' . number_format($product->get_stock_quantity(),0,'','') . ' in stock</div>'; 
		}
	}
}

add_action('woocommerce_after_shop_loop_item','show_stock', 1);
#stock quantity end


#Disable Payment Gateway for whole saler return filter to true if user is not whole saler
if ( wc_current_user_has_role( 'whole_saler' ) ) {
add_filter( 'woocommerce_cart_needs_payment', '__return_false' );
	
add_filter( 'woocommerce_package_rates', 'shipping_methods_based_on_wholesale_customer', 10, 2 );
function shipping_methods_based_on_wholesale_customer( $rates, $package ){
    $is_wholesale = get_user_meta( get_current_user_id(), 'whole_saler', true );
    
    // Set the shipping methods rate ids in the arrays:
    if( $is_wholesale ) {
        $shipping_rates_ids = array('local_pickup:2', 'flat_rate:3', 'flat_rate:4', 'flat_rate:5');
		// To be removed for NON Wholesale users
    } else {
        $shipping_rates_ids = array('AUS_PARCEL_REGULAR', 'AUS_PARCEL_EXPRESS'); 
		// To be removed for Wholesale users
    }

    // Loop through shipping rates fro the current shipping package
    foreach( $rates as $rate_key => $rate ) {
        if ( in_array( $rate_key, $shipping_rates_ids) ) {
            unset( $rates[$rate_key] ); 
        }
    }
    
    return $rates;
}
	
}
else
{
add_filter( 'woocommerce_cart_needs_payment', '__return_true' );
}
#Disable Payment Gateway end

#allow shop manager to edit whole saler account
function wws_allow_shop_manager_role_edit_capabilities( $roles ) {
    $roles[] = 'whole_saler'; // whole_saler value
    return $roles;
}
add_filter( 'woocommerce_shop_manager_editable_roles', 'wws_allow_shop_manager_role_edit_capabilities' );
#allow shop manager to edit whole saler account END

#add filter admin bar in front end for shop staff user
if ( wc_current_user_has_role( 'shop_staff' ) ) {
add_filter( 'show_admin_bar', '__return_true' , 1000 );
}

#Disable local pickup and flat rate for customer
if ( wc_current_user_has_role( 'customer' ) ) {
add_filter( 'woocommerce_package_rates', 'shipping_methods_based_on_retail_customer', 10, 2 );
function shipping_methods_based_on_retail_customer( $rates, $package ){
    $is_retail = get_user_meta( get_current_user_id(), 'customer', true );
    
    // Set the shipping methods rate ids in the arrays:
    if( $is_retail ) {
        $shipping_rates_ids = array('AUS_PARCEL_REGULAR', 'AUS_PARCEL_EXPRESS');
		// To be removed for NON retail users
    } else {
        $shipping_rates_ids = array('local_pickup:2', 'flat_rate:3', 'flat_rate:4', 'flat_rate:5' ); 
		// To be removed for retail users
    }

    // Loop through shipping rates from the current shipping package
    foreach( $rates as $rate_key => $rate ) {
        if ( in_array( $rate_key, $shipping_rates_ids) ) {
            unset( $rates[$rate_key] ); 
        }
    }
    
    return $rates;
}
	
}
#Disable local pickup and flat rate for customer end

#add filter admin bar in front end for shop staff user END

#
#
#
#
# CRON SCHEDULE FOR QUOTE EXPIRATION.
#
#
#
#
#

// function my_cron_schedules($schedules){

//     if(!isset($schedules["5min"])){
//         $schedules["5min"] = array(
//             'interval' => 5*60,
//             'display' => __('Once every 5 minutes'));
//     }

//     return $schedules;

// }
// add_filter('cron_schedules','my_cron_schedules');

// if (!wp_next_scheduled('my_task_hook')) {
//         wp_schedule_event( time(), '5min', 'my_task_hook' );
//     }
// }

// add_action ( 'my_task_hook', 'my_task_function' );

// function my_task_function() {
//     echo 'I have been called to action. I will do the same next week';
// }

#
#
#
# END OF CRON
#
#