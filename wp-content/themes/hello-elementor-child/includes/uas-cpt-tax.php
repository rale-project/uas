<?php

add_action( 'init', function () {

    // Utility functions for generating Post Type and Taxonomy Labels

    $gen_cpt_labels = function($single, $plural, $menu_name){

        return array(
            "name"                => _x( $plural, "Post Type General Name", "uas" ),
            "singular_name"       => _x( $single, "Post Type Singular Name", "uas" ),
            "menu_name"           => __( $menu_name, "uas" ),
            "parent_item_colon"   => __( "Parent $single", "uas" ),
            "all_items"           => __( "All $plural", "uas" ),
            "view_item"           => __( "View $single", "uas" ),
            "add_new_item"        => __( "Add New $single", "uas" ),
            "add_new"             => __( "Add New", "uas" ),
            "edit_item"           => __( "Edit $single", "uas" ),
            "update_item"         => __( "Update $single", "uas" ),
            "search_items"        => __( "Search $single", "uas" ),
            "not_found"           => __( "Not Found", "uas" ),
            "not_found_in_trash"  => __( "Not found in Trash", "uas" ),
        );
    };

    $gen_tax_labels = function($single, $plural, $menu_name){
        return array(
            "name"                       => _x( "$plural", "$single General Name", "uas" ),
            "singular_name"              => _x( "$single", "$single Singular Name", "uas" ),
            "menu_name"                  => __( $menu_name, "uas" ),
            "all_items"                  => __( "All $plural", "uas" ),
            "parent_item"                => __( "Parent $single", "uas" ),
            "parent_item_colon"          => __( "Parent $single:", "uas" ),
            "new_item_name"              => __( "New $single Name", "uas" ),
            "add_new_item"               => __( "Add New $single", "uas" ),
            "edit_item"                  => __( "Edit $single", "uas" ),
            "update_item"                => __( "Update $single", "uas" ),
            "view_item"                  => __( "View $single", "uas" ),
            "separate_items_with_commas" => __( "Separate $plural with commas", "uas" ),
            "add_or_remove_items"        => __( "Add or remove $plural", "uas" ),
            "choose_from_most_used"      => __( "Most used $plural", "uas" ),
            "popular_items"              => __( "Popular $plural", "uas" ),
            "search_items"               => __( "Search $plural", "uas" ),
            "not_found"                  => __( "Not Found", "uas" ),
            "no_terms"                   => __( "No $single", "uas" ),
            "items_list"                 => __( "$plural list", "uas" ),
            "items_list_navigation"      => __( "$plural list navigation", "uas" ),
        );
    };

    // Common Post Type Args (usually always the same)

    $cpt_common_args = array(
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page'
    );

    // Common Post Type Taxonomy Args (usually always the same)

    $tax_common_args = array(
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true
    );


    // --------
    // SERVICES
    // --------

    // $services_args = array(
    //     'label'               => __( 'Services', 'uas' ),
    //     'description'         => __( 'Services', 'uas' ),
    //     'labels'              => $gen_cpt_labels('Service', 'Services', 'Services'),
    //     'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields', 'elementor'),
    //     //'taxonomies'          => array( 'services_types' ),
    //     'menu_icon'           => 'dashicons-format-aside',
    //     'has_archive'         => false,
    // );
        
    // register_post_type( 'services', array_merge($cpt_common_args, $services_args));


    // // -------------
    // // PROJECT TYPES
    // // -------------

    // $project_type_tax_args = array(
    //     'labels' => $gen_tax_labels('Project Type', 'Project Types', 'Project Types'),
    // );

    // register_taxonomy( 'project_types', array( 'projects' ), array_merge($tax_common_args, $project_type_tax_args ));

    // -------------
    // SERVICE TYPES
    // -------------

    //$service_type_tax_args = array(
    //    'labels' => $gen_tax_labels('Service Type', 'Service Types', 'Service Types'),
    //);

    //register_taxonomy( 'service_types', array( 'services' ), array_merge($tax_common_args, $service_type_tax_args ));





}, 0);


// Register Custom Post Type
function cpt_uas_trade_user_pricing_tier() {

    $labels = array(
        'name'                  => _x( 'Trade User Price Tier', 'Post Type General Name', 'uas_domain' ),
        'singular_name'         => _x( 'Trade User Price Tier', 'Post Type Singular Name', 'uas_domain' ),
        'menu_name'             => __( 'Trade User Price Tier', 'uas_domain' ),
        'name_admin_bar'        => __( 'Trade User Price Tier', 'uas_domain' ),
        'archives'              => __( 'Item Archives', 'uas_domain' ),
        'attributes'            => __( 'Item Attributes', 'uas_domain' ),
        'parent_item_colon'     => __( 'Parent Item:', 'uas_domain' ),
        'all_items'             => __( 'All Items', 'uas_domain' ),
        'add_new_item'          => __( 'Add New Item', 'uas_domain' ),
        'add_new'               => __( 'Add New', 'uas_domain' ),
        'new_item'              => __( 'New Item', 'uas_domain' ),
        'edit_item'             => __( 'Edit Item', 'uas_domain' ),
        'update_item'           => __( 'Update Item', 'uas_domain' ),
        'view_item'             => __( 'View Item', 'uas_domain' ),
        'view_items'            => __( 'View Items', 'uas_domain' ),
        'search_items'          => __( 'Search Item', 'uas_domain' ),
        'not_found'             => __( 'Not found', 'uas_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'uas_domain' ),
        'featured_image'        => __( 'Featured Image', 'uas_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'uas_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'uas_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'uas_domain' ),
        'insert_into_item'      => __( 'Insert into item', 'uas_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'uas_domain' ),
        'items_list'            => __( 'Items list', 'uas_domain' ),
        'items_list_navigation' => __( 'Items list navigation', 'uas_domain' ),
        'filter_items_list'     => __( 'Filter items list', 'uas_domain' ),
    );
    $args = array(
        'label'                 => __( 'Trade User Price Tier', 'uas_domain' ),
        'description'           => __( 'List all Trade Users that has Price Tier', 'uas_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title' ),
        'hierarchical'          => false,
        'public'                => false,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 99,
        'menu_icon'             => 'dashicons-admin-users',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => false,
        'capability_type'       => 'page',
    );
    register_post_type( 'tradeuser_price_tier', $args );

}
add_action( 'init', 'cpt_uas_trade_user_pricing_tier', 0 );