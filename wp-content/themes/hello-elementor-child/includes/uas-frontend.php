<?php

add_action( 'wp_enqueue_scripts', function ()
{
    // --------------------------
    // GLOBAL SCRIPTS (SITE-WIDE)
    // --------------------------

	// Enqueue Child theme style (DO NOT REMOVE THIS LINE)
    if( is_front_page() ){
        wp_enqueue_style('slick-css', 'http://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', ['hello-elementor'], '2.0.0');
    }
    wp_enqueue_style('fancybox-3', 'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css', '', '1.0.0');

    wp_enqueue_style('hello-elementor-child', get_stylesheet_directory_uri() . '/style.css', ['hello-elementor'], '1.0.0');
    
    // Add Additional JS Libraries
    wp_enqueue_script('jquerynpm', 'https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js', '', '1.0.0');

    wp_enqueue_script('fancyboxjs', 'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js', '', '1.0.0');

	// Back to Top init, other description, other description
	wp_enqueue_script('uas-frontend', get_stylesheet_directory_uri() . '/assets/js/uas-frontend.js', ['jquery'], '1.0.0');
    
    $current_user = wp_get_current_user();

    if( is_user_logged_in() ){

        $user_det = array(
            'user_nicename'     => $current_user->user_nicename,
            'dashboard_link'    => site_url() . '/dashboard/' ,
            'user_role'         => $current_user->roles[0] 
        );
        
        $userdata = json_encode($user_det);
        wp_localize_script( 'uas-frontend', 'getUserDetails', $userdata );

    }


    // --------------------------
    // PER PAGE/POST-TYPE SCRIPTS
    // --------------------------

    /*

    if(is_page('xx')){
        // wp_enqueue_script('some-library', get_stylesheet_directory_uri() . '/assets/js/page-specific.js', ['jquery'], '1.0.0');
    }

    if(is_singular('some_cpt_slug')){
        // wp_enqueue_script('some-library', get_stylesheet_directory_uri() . '/assets/js/cpt-specific.js', ['jquery'], '1.0.0');
    }

    // -----------------------------
    // PER POST-TYPE ARCHIVE SCRIPTS
    // -----------------------------

    if(is_post_type_archive('some_cpt_slug')){
        // wp_enqueue_script('some-library', get_stylesheet_directory_uri() . '/assets/js/archive-cpt-specific.js', ['jquery'], '1.0.0');
    }

    */

    wp_enqueue_script('match-height-js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js', ['jquery'], '1.0.0');
    

    if( is_front_page() ){
        wp_enqueue_script('slick-js-lib', 'http://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', ['jquery'], '1.2.0');
        wp_enqueue_script('custom-script', get_stylesheet_directory_uri() . '/assets/js/uas-frontend.js', ['jquery'], '1.0.0');
    }
});

function admin_styles_scripts(){

    wp_register_style( 'admin_custom_css', get_stylesheet_directory_uri() . '/assets/css/admin/admin-styles.css', false, '1.0.0' );
    wp_enqueue_style( 'admin_custom_css' );

    wp_enqueue_script('admin_custom_js', get_stylesheet_directory_uri() . '/assets/css/admin/admin-scripts.js', '', '1.0.0');

}
add_action( 'admin_enqueue_scripts', 'admin_styles_scripts' );