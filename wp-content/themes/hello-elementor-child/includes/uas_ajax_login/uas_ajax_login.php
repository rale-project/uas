<?php

/**
 * Ajax Login
 * 
 */

namespace UAS_Ajaxlogin;

// [rb_ajax_login] - Shortcode to for outputting an ajax login form

if(!class_exists('RB_Ajax_Login')){
class RB_Ajax_Login {

    protected static $_instance = null;
    protected $slug = 'rb_ajax_login';

    public static function get_instance(){
        if(self::$_instance == null){
            self::$_instance = new self();
            return self::$_instance;
        } else return self::$_instance;
    }

    public function __construct(){
        $this->setup();
    }

    public function setup()
    {
        add_action("wp_ajax_nopriv_{$this->slug}", [&$this, "attempt_login"]);
        add_shortcode($this->slug, [&$this, "render"]);
    }

    public function render($atts, $content = null)
    {
        $uri  = get_stylesheet_directory_uri();
        $path = get_stylesheet_directory();
        $slug = $this->slug;
        $svg = "<svg class='loader hide' fill='#dddddd' viewBox='0 0 35 35' xmlns='http://www.w3.org/2000/svg'><path opacity='.2' d='M20.201 5.169c-8.254 0-14.946 6.692-14.946 14.946 0 8.255 6.692 14.946 14.946 14.946s14.946-6.691 14.946-14.946c-.001-8.254-6.692-14.946-14.946-14.946zm0 26.58c-6.425 0-11.634-5.208-11.634-11.634 0-6.425 5.209-11.634 11.634-11.634 6.425 0 11.633 5.209 11.633 11.634 0 6.426-5.208 11.634-11.633 11.634z'/><path d='M26.013 10.047l1.654-2.866a14.855 14.855 0 0 0-7.466-2.012v3.312c2.119 0 4.1.576 5.812 1.566z'><animateTransform attributeType='xml' attributeName='transform' type='rotate' from='0 20 20' to='360 20 20' dur='0.5s' repeatCount='indefinite'/></path></svg>";

        // wrapper start
        $html .= "<div class='$slug'>";

        // enqueue style
        if (class_exists('Elementor\Plugin') && \Elementor\Plugin::$instance->editor->is_edit_mode() ) {
            $html .= '<style>' . file_get_contents("$path/includes/$slug/$slug.css") . '</style>';
        } else {
            wp_enqueue_style($slug, "$uri/includes/$slug/$slug.css");
        }
    

        if(!wp_script_is('enqueued')){
            wp_enqueue_script($slug, "$uri/includes/$slug/$slug.js");
            wp_localize_script($slug, $slug, [
                'url'    => admin_url('admin-ajax.php'),
                'action' => $slug,
                'nonce'  => wp_create_nonce($slug),
                'slug'   => $slug
            ]);
        }

        $options = shortcode_atts([
            'container' => true,
            'btn_txt'   => 'Login',
            'user_ph'   => 'Username',
            'pass_ph'   => 'Password',
            'mail_ph'   => 'Your Email Address',
            'reset_link'=> 'true',
            'redirect'  => get_site_url()
        ], $atts, $slug);

        // container start
        if($options['container'] == true){
            if($options['fullwidth'] == true){
                $html .= "<div class='container-full'>";
            } else {
                $html .= "<div class='container'>";
            }
        }

        $html .=
            "<div class='grid'>";

            if(is_user_logged_in()){

                $html .= "<div class='col-md-12'><div class='{$slug}_status'>You are already logged in.</div>";
                $html .= "<div><a href='" . wp_logout_url( home_url() ) . "'>Log out?</a></div>";

            } else {

                $html .=
                    "<div class='col-md-12 tlite_validate user' data-tlite='s' title='Username is required'>" .
                        "<input type='text' class='{$slug}_user' name='{$slug}_user' placeholder='{$options['user_ph']}'/>" .
                    "</div>";
                $html .= 
                    "<div class='col-md-12 tlite_validate pass' data-tlite='s' title='Password is required'>" . 
                        "<input type='password' class='{$slug}_pass' name='{$slug}_pass' placeholder='{$options['pass_ph']}'/>" .
                    "</div>";
                $html .= 
                    "<div class='col-md-12 rem'>" . 
                        "<label><input type='checkbox' class='{$slug}_rem' name='{$slug}_rem'/>Remember Me</label>" . 
                    "</div>";
                $html .=
                    "<div class='col-md-12'><button type='button' class='{$slug}_btn'  name='{$slug}_btn'>" . 
                        "$svg{$options['btn_txt']}</button>" . 
                    "</div>";
                $html .=
                    "<div class='col-md-12'><div class='{$slug}_status'></div></div>";
                if($options['reset_link'] == 'true'){
                    $html .=
                    "<div class='col-md-12'>" . 
                        "<div class='{$slug}_reset'>" . 
                            "<a href='" . wp_lostpassword_url( home_url() ) . "'>Forgot Password?</a>" . 
                        "</div>" . 
                    "</div>";
                }

            }

        $html .=
            "</div>";

        // container end
        if($options['container'] == true){
            $html .= "</div>";
        }

        // wrapper end
        $html .= "</div>";
        return $html;
    }
    
    public function attempt_login()
    {
        // verify
        if(isset($_POST['nonce']) && wp_verify_nonce($_POST['nonce'], $this->slug)){

            // sanitize
            $user = isset($_POST['user']) ? sanitize_text_field($_POST['user']) : '';
            $pass = isset($_POST['pass']) ? sanitize_text_field($_POST['pass']) : '';
            $rem  = isset($_POST['rem'])  ? true : false;

            if(!$user){
                wp_send_json_success([
                    'status' => 'user'
                ]); wp_die();
            }

            if(!$pass){
                wp_send_json_success([
                    'status' => 'pass'
                ]); wp_die();
            }

            $result = wp_signon([
                'user_login'    => $user,
                'user_password' => $pass,
                'remember'      => $rem
            ], is_ssl() );
         
            if ( !is_wp_error( $result ) ) {
                // Success
                wp_send_json_success([
                    'status' => 'success',
                    'redirect'=> get_site_url()
                ]); wp_die();
            } else {
                // Failure
                $msg = $result->get_error_message();
                wp_send_json_success([
                    'status' => 'mcfail',
                    'msg' => $msg
                ]); wp_die();
            }

        } else wp_send_json_error('comms'); wp_die();
    }  
} // end class
} // end if