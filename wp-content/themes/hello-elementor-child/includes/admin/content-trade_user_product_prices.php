<div class="trade_user_product_price_wrapper">
	
	<h2>Product Categories</h2>

	<ul class="product_cat-wrapper">
	<?php 
		$taxonomy     = 'product_cat';
		$orderby      = 'name';  
		$show_count   = 0;      // 1 for yes, 0 for no
		$pad_counts   = 0;      // 1 for yes, 0 for no
		$hierarchical = 1;      // 1 for yes, 0 for no  
		$title        = '';  
		$empty        = 0;

		$args = array(
		     'taxonomy'     => $taxonomy,
		     'orderby'      => $orderby,
		     'show_count'   => $show_count,
		     'pad_counts'   => $pad_counts,
		     'hierarchical' => $hierarchical,
		     'title_li'     => $title,
		     'hide_empty'   => $empty
		);
		$all_categories = get_categories( $args );

		$user_id = $get_user_id_trade;

		if( $all_categories ){
			foreach ( $all_categories as $cat ) {
				
				if( $cat->category_parent == 0 ){

					$category_id = $cat->term_id;

					$args2 = array(
			                'taxonomy'     => $taxonomy,
			                'child_of'     => 0,
			                'parent'       => $category_id,
			                'orderby'      => $orderby,
			                'show_count'   => $show_count,
			                'pad_counts'   => $pad_counts,
			                'hierarchical' => $hierarchical,
			                'title_li'     => $title,
			                'hide_empty'   => $empty
			        );

			        $sub_cats = get_categories( $args2 );

			        $args_tier = array(
									'post_type'			=> 'tradeuser_price_tier',
									'posts_per_page'	=> -1,
								);

			        $get_tier_prices = get_posts($args_tier);

					echo '<li>';
						if($sub_cats) {
							echo '<a href="#" class="has-subcat_prod">'. $cat->name .'<span class="ctm-arrow downcarret"></span>';
							echo '</a>';
						}else{
							echo $cat->name;
						}

						$option_name  = '_' . $cat->slug . '_trade_user';
						$selected_val = get_user_meta( $user_id, $option_name, true );

					?>
						<div class="select_tier__wrapper">
							<select name="_<?php echo $cat->slug . '_trade_user' ?>">
								<option value=" ">
									Select Tier Prices
								</option>
								<?php 
									foreach( $get_tier_prices as $tier ){
								?>
										<option value="<?php echo $tier->post_name; ?>" <?php selected( $selected_val,$tier->post_name ); ?> >
											<?php echo $tier->post_title; ?>
										</option>
								<?php
									}
								?>
							</select>
						</div>
					<?php

			        if($sub_cats) {

			        	echo '<ul class="sub_cat_product">';
			            foreach($sub_cats as $sub_category) {
			                echo '<li>';
			                	echo  $sub_category->name;

			                	$option_name  = '_' . $sub_category->slug . '_trade_user';
								$selected_val = get_user_meta( $user_id, $option_name, true );
                	?>
                			<div class="select_tier__wrapper">
								<select name="_<?php echo $sub_category->slug . '_trade_user' ?>">
									<option value=" ">
										Select Tier Prices
									</option>
									<?php 
										foreach( $get_tier_prices as $tier ){
									?>
											<option value="<?php echo $tier->post_name; ?>" <?php selected( $selected_val,$tier->post_name ); ?>><?php echo $tier->post_title; ?></option>
									<?php
										}
									?>
								</select>
							</div>
                	<?php
		                	echo '</li>';
			            }
			            echo '</ul>';   
			        }
			        echo '</li>';
				}

			}
		}

	?>
	</ul>
</div>