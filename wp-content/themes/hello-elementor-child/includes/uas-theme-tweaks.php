<?php

add_shortcode('advertisement_display_per_user','get_advertisement_per_user_tier');
function get_advertisement_per_user_tier(){

	$current_user = get_current_user_id();

	$taxonomy     = 'product_cat';
	$orderby      = 'name';  
	$show_count   = 0;      // 1 for yes, 0 for no
	$pad_counts   = 0;      // 1 for yes, 0 for no
	$hierarchical = 1;      // 1 for yes, 0 for no  
	$title        = '';  
	$empty        = 0;

	$args = array(
	     'taxonomy'     => $taxonomy,
	     'orderby'      => $orderby,
	     'show_count'   => $show_count,
	     'pad_counts'   => $pad_counts,
	     'hierarchical' => $hierarchical,
	     'title_li'     => $title,
	     'hide_empty'   => $empty
	);
	$all_categories = get_categories( $args );

	$temp_array 		= array();
	$temp_user_tier 	= array();
	
	if( is_user_logged_in() ){

		if( $all_categories ){
			
			$ctr 		= 0;
			$ctr_user 	= 0;

			foreach ($all_categories as $category ) {
				
				$option_name  			= '_' . $category->slug . '_trade_user';
				$user_has_tier			= get_user_meta( $current_user, $option_name, true  );
				$temp_array[$ctr] 		= $category->slug;
				
				if( strlen($user_has_tier) > 1  ){
					$post = get_page_by_path( $user_has_tier, OBJECT, 'tradeuser_price_tier' );
					$temp_user_tier[$ctr_user] = $post->ID;
					$ctr_user++;
				}

				$ctr++;

			}

		}

		$user_current_tier = array_unique( $temp_user_tier );

		ob_start();
		
		echo "<div class='advertisement_wrapper'>";

			foreach ($user_current_tier as $tier ) {
				$the_field = get_field('advertisement',$tier);

				if( $the_field ){
					foreach ($the_field as $data) {
						echo '<div class="advertise-image">';
						$the_link = '#';
						if( $data['link'] ){
							$the_link = $data['link'];
						}
							echo '<a href="'. $the_link .'">';
								echo '<img src="'. $data['image']['url'] .'">';
							echo '</a>';
						echo '</div>';
					}
				}

			}

		echo '</div>';

		$content = ob_get_contents();
		wp_reset_query();
		ob_end_clean();

	}

	return $content;
}

add_shortcode('get_user_logged_in','get_user_logged_in');
function get_user_logged_in(){

	$current_user = wp_get_current_user();
	
	if( is_user_logged_in() ){
		ob_start();
		echo '<div class="is_loggedin_user">';	
		printf(
			/* translators: 1: user display name 2: logout url */
			__( 'Welcome! %1$s <div class="notuserloggeditout">not %1$s? <a href="%2$s">Log out</a></div>', 'woocommerce' ),
			'<strong>' . esc_html( $current_user->data->display_name ) . '</strong>',
			esc_url( wp_logout_url( site_url() ) )
		);
		echo '</div>';
		$content = ob_get_contents();
		wp_reset_query();
		ob_end_clean();
	}

	return $content;
}

add_shortcode('list_of_all_orders','get_all_orders');
function get_all_orders(){

	ob_start();

	    wc_get_template(
	      'myaccount/my-orders.php'
	    );

	$content = ob_get_contents();
	wp_reset_query();
	ob_end_clean();


	return $content;
	
}

add_action('wp_ajax_load_view_order','load_view_order_by_id');
add_action('wp_ajax_nopriv_load_view_order','load_view_order_by_id');
function load_view_order_by_id(){

	$order_id = sanitize_text_field($_GET['order_number']);

	$order = wc_get_order( $order_id );

	ob_start(); 
	wc_get_template(
      'myaccount/view-order.php',
      array(
        'status'   => $status, // @deprecated 2.2.
        'order'    => $order,
        'order_id' => $order->get_id(),
      )
    );

    $content = ob_get_contents();
	wp_reset_query();
	ob_end_clean();

	$result['html'] = $content; 

	echo $content;

	die();
}

function custom_redirects() {
 
    $current_user = wp_get_current_user();

    if( $current_user->roles[0] == 'administrator' ){
    	return;
    }

	if( !is_user_logged_in() ){

		if( is_page(690) || is_page(734) || is_page(715) ){
			wp_redirect( home_url() );
			die;
		}

	}

}
add_action( 'template_redirect', 'custom_redirects' );

function update_order_status_per_view(){

	if( isset($_POST['orderUpdateStatus']) ){

		$get_selected_order_status = sanitize_text_field($_POST['select-updated-status']);
		$get_order_id = sanitize_text_field($_POST['the_order_id']);

		if( $get_selected_order_status ){
			$order = wc_get_order( $get_order_id );
			$get_selected_order_status = str_replace("wc-", "", $get_selected_order_status);
			$order->update_status($get_selected_order_status);
		}

	}

}
add_action('init','update_order_status_per_view');


function add_body_class_user_trade( $classes ){

	if( is_user_logged_in() && wp_get_current_user('whole_saler') ){
		return array_merge( $classes, array( 'logged_as_trade_user' ) );
	}	

}
add_filter('body_class','add_body_class_user_trade');


function add_post_table_trade_user( $column ) {
    $column['wholesaler_col']  = 'Approve Wholesaler';
 
    return $column;
}
 
add_filter( 'manage_users_columns', 'add_post_table_trade_user' );

function modify_user_wholesaler_table_row( $val, $column_name, $user_id ) {

	$user_meta = get_userdata($user_id);

	$user_roles = $user_meta->roles[0];


    if( $user_roles == 'whole_saler' ){

    	$user = get_user_meta($user_id,'approve_trade_user');

	    if( $user ){
	    	$approved_user = 'Approved';
	    }else{
	    	$approved_user = 'Pending';
	    }
    }


    switch ($column_name) {
        case 'wholesaler_col' :
            return $approved_user;
            break;

        default:
    }

    return $return;
}

add_filter( 'manage_users_custom_column', 'modify_user_wholesaler_table_row', 10, 3 );

function wpdocs_my_login_redirect( $url, $request, $user ) {

	$user_meta = get_userdata($user->ID);

	$user_roles = $user_meta->roles[0];

    if( $user && is_object($user) ){

		if( $user_roles == 'whole_saler' ){
			$approved_user = get_user_meta($user->ID,'approve_trade_user');

			if( !$approved_user ){
				wp_logout();
				$url = wp_login_url() . '/?status=pending';
				 
			}
		}

    }
	return $url;
}
 
add_filter( 'login_redirect', 'wpdocs_my_login_redirect', 10, 3 );

add_action('login_head','ref_access');

function ref_access() {

    global $error;

    if( !empty($_GET['status']) && 'pending' == $_GET['status'] )
        $error  = 'Your account is still pending. Please wait for the UAS-ADMIN for approval.';
}

function add_logout_link_hidden(){
	echo '<a href="' . wp_logout_url( site_url() ) . '" class="hide hidden-logout-footer-link" ></a> ';
}
add_action('wp_footer','add_logout_link_hidden');