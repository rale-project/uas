<?php

// ============
// SPEED TWEAKS
// ============

// -----------------------------------
// jQuery from Google's CDN done right
// -----------------------------------

add_action('init', 'uas_load_jquery_from_cdn');

function uas_load_jquery_from_cdn() {
	if (is_admin()) {
		return;
	}

	global $wp_scripts;
	if (isset($wp_scripts->registered['jquery']->ver)) {
		$ver = $wp_scripts->registered['jquery']->ver;
                $ver = str_replace("-wp", "", $ver);
	} else {
		$ver = '1.12.4';
	}

	wp_deregister_script('jquery');
	wp_register_script('jquery', "//ajax.googleapis.com/ajax/libs/jquery/$ver/jquery.min.js", false, $ver);
}

// ---------------------
// Remove JQuery migrate
// ---------------------

// Rationale: Most up-to-date frontend code and plugins don’t require jquery-migrate.min.js.
// More often than not, keeping this simply adds unnecessary load.

add_action('wp_default_scripts', 'uas_remove_jquery_migrate');

function uas_remove_jquery_migrate( $scripts ) {
	if (!is_admin() && isset($scripts->registered['jquery'])) {
    $script = $scripts->registered['jquery'];
    
    if ($script->deps) { // Check whether the script has any dependencies
      $script->deps = array_diff($script->deps, array(
          'jquery-migrate'
      ));
    }
  }
}
// -----------------
// Disable Gutenberg
// -----------------

add_filter('use_block_editor_for_post', '__return_false', 10);
add_filter('use_block_editor_for_post_type', '__return_false', 10);
add_action('wp_enqueue_scripts', 'uas_disable_gutenberg');

function uas_disable_gutenberg() {
	wp_dequeue_style( 'wp-block-library' );
}

// ------------------
// Remove Header Junk
// ------------------

add_action( 'after_setup_theme', 'uas_after_setup_theme' );
function uas_after_setup_theme() {
	// remove Really Simple Discovery
	remove_action('wp_head', 'rsd_link');
	// remove Windows Live Writer
	remove_action('wp_head', 'wlwmanifest_link');
	// remove emoji scripts
	remove_action('wp_head', 'print_emoji_detection_script', 7 ); 
	remove_action('wp_print_styles', 'print_emoji_styles' ); 
	remove_action('admin_print_styles', 'print_emoji_styles' );
	remove_action('admin_print_scripts', 'print_emoji_detection_script' ); 
	remove_filter('wp_mail', 'wp_staticize_emoji_for_email' );
	remove_filter('the_content_feed', 'wp_staticize_emoji' );
	remove_filter('comment_text_rss', 'wp_staticize_emoji' );
	remove_filter('the_content', 'convert_smilies');
	add_filter('emoji_svg_url', '__return_false' );
	add_filter('tiny_mce_plugins', 'uas_remove_tiny_mce');
  // remove Rss feed links
  remove_action( 'wp_head', 'feed_links_extra', 3 );
  remove_action( 'wp_head', 'feed_links', 2 );    
	// remove generator tag
	add_filter('the_generator', 'uas_remove_generator_tag');	
	remove_action('wp_head', 'wp_generator');	
	// remove xml+oembed and json+oembed links
	remove_action('template_redirect', 'rest_output_link_header', 11, 0);
	remove_action('wp_head', 'rest_output_link_wp_head');
	// Turn off oEmbed auto discovery.
	add_filter('embed_oembed_discover', '__return_false' );
	// Don't filter oEmbed results.
	remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10 );
	// Remove oEmbed discovery links.
	remove_action('wp_head', 'wp_oembed_add_discovery_links' );
	// Remove oEmbed-specific JavaScript from the front-end and back-end.
	remove_action('wp_head', 'wp_oembed_add_host_js' );
	// remove shortlinks
	remove_action('wp_head', 'wp_shortlink_wp_head' );
	// remove prefetch links ****
	remove_action('wp_head', 'wp_resource_hints', 2 );
	// remove rel next/prev for posts
	remove_filter('wp_head','adjacent_posts_rel_link_wp_head',10);
}

function uas_remove_generator_tag() {
	return '';
}
function uas_remove_tiny_mce($p){
	if(is_array($p)) {
		return array_diff($p,['wpemoji']);
	} else { return[]; }
}
// --------------------------
// Remove Comments Completely
// --------------------------

// Disable support for comments and trackbacks in post types
add_action('admin_init', function () {
	$post_types = get_post_types();
	foreach ($post_types as $post_type) {
		if(post_type_supports($post_type, 'comments')) {
			remove_post_type_support($post_type, 'comments');
			remove_post_type_support($post_type, 'trackbacks');
		}
	}
});
// Close comments on the front-end
function rb_disable_comments_status() {
	return false;
}
add_filter('comments_open', 'rb_disable_comments_status', 20, 2);
add_filter('pings_open', 'rb_disable_comments_status', 20, 2);

// Hide existing comments
add_filter('comments_array', function ($comments) {
	$comments = array();
	return $comments;
}, 10, 2);

// Remove comments page in menu
add_action('admin_menu', function () {
	remove_menu_page('edit-comments.php');
});

// Redirect any user trying to access comments page
add_action('admin_init', function () {
	global $pagenow;
	if ($pagenow === 'edit-comments.php') {
		wp_redirect(admin_url()); exit;
	}
});

// Remove comments metabox from dashboard
add_action('admin_init', function () {
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
});

// Remove comments links from admin bar
add_action('init', 'uas_remove_comments');

function uas_remove_comments() {
	if (is_admin_bar_showing()) {
		remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
	}
}

// -------------------------
// Other Misc. Optimizations
// -------------------------

// Disable Hello Elementor Parent Theme 'theme.css' Stylesheet (not needed)
add_filter('hello_elementor_enqueue_theme_style', '__return_false', 10);
 
// Adjust JPEG Compression quality for newly uploaded Imags - 75% Quality. 
add_filter('jpeg_quality', 'uas_jpeg_quality');

function uas_jpeg_quality($arg) {
	return 75;
}

// Back to Top button
add_action('wp_footer', 'uas_back_to_top');

function uas_back_to_top() {
	echo '<a href="javascript:" id="back-to-top"><i class="fa fa-chevron-up"></i></a>';
}
// --------------------
// Disable Auto Updates
// --------------------

/*
 * Disable All Automatic Updates
 * 3.7+
 *
 * @author	sLa NGjI's @ slangji.wordpress.com
*/
add_filter( 'auto_update_translation', '__return_false' );
add_filter( 'automatic_updater_disabled', '__return_true' );
add_filter( 'allow_minor_auto_core_updates', '__return_false' );
add_filter( 'allow_major_auto_core_updates', '__return_false' );
add_filter( 'allow_dev_auto_core_updates', '__return_false' );
add_filter( 'auto_update_core', '__return_false' );
add_filter( 'wp_auto_update_core', '__return_false' );
add_filter( 'auto_core_update_send_email', '__return_false' );
add_filter( 'send_core_update_notification_email', '__return_false' );
add_filter( 'auto_update_plugin', '__return_false' );
add_filter( 'auto_update_theme', '__return_false' );
add_filter( 'automatic_updates_send_debug_email', '__return_false' );
add_filter( 'automatic_updates_is_vcs_checkout', '__return_true' );

add_filter( 'automatic_updates_send_debug_email ', '__return_false', 1 );
if( !defined( 'AUTOMATIC_UPDATER_DISABLED' ) ) define( 'AUTOMATIC_UPDATER_DISABLED', true );
if( !defined( 'WP_AUTO_UPDATE_CORE') ) define( 'WP_AUTO_UPDATE_CORE', false );