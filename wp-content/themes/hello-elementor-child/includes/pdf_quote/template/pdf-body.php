<?php 
	$order = wc_get_order($order_id);
	//get user info
	$user_id = get_post_meta( $order_id, '_customer_user', true );
	$customer = new WC_Customer( $user_id );
	$first_name = $customer->get_first_name();
	$last_name = $customer->get_last_name();
	$billing_company = $customer->get_billing_company();
 	$quote_expiry = get_field('date_expiry',$order->get_order_number() );
?>
<div style="display:inline-block; width:100%; padding-top:150px;">
	<div class="title">
		<div>
			<h1>Quote</h1>
			<span>Quote Number : <?php echo $order_id; ?></span>
			<p>Date : <?php echo $date_pdf; ?></p>
			<p>Quote expiry date : <?php echo $quote_expiry; ?></p>			
			<p>Name : <?php echo "$first_name $last_name"; ?></p>
			<p>Company : <?php echo "$billing_company"; ?></p>			
		</div>
	</div>		
</div>

<table cellpadding="0" cellspacing="0" style="width:100%; display:inline-block; padding-top:20px;">
	<thead>
		<tr style="text-align:left;">
			<th style="text-align:left;background-color:#1B3564;color:#fff;padding-left:10px;padding-top:5px;padding-bottom:5px;text-align:center;">Product</th>
			<th style="text-align:left;background-color:#1B3564;color:#fff;display:inline-block;padding-top:5px;text-align:center;padding-bottom:5px;">Total</th>
		</tr>
	</thead>
</table>
<table cellpadding="0" cellspacing="0" style="width:100%; display:inline-block;" >
	<thead></thead>
	<tbody>
		<?php

		$order_items = $order->get_items();
		foreach ( $order_items as $item_id => $item ) {
			$product = $item->get_product();

			wc_get_template(
				'order/order-details-item.php',
				array(
					'order'              => $order,
					'item_id'            => $item_id,
					'item'               => $item,
					'show_purchase_note' => $show_purchase_note,
					'purchase_note'      => $product ? $product->get_purchase_note() : '',
					'product'            => $product,
				)
			);
		}

		?>
	</tbody>
</table>
<table cellpadding="0" cellspacing="0" style="display:block; width:50%; padding-left:20px;margin-left:450px; " >
		<tfoot>
			<?php
				foreach ( $order->get_order_item_totals() as $key => $total ) {
					?>
						<tr>
							<th scope="row" style="padding-top:15px;">
								
								<?php echo esc_html( $total['label'] ); ?>

							</th>
							<td style="padding-top:15px;padding-left:35px;">
								<?php echo ( 'payment_method' === $key ) ? esc_html( $total['value'] ) : wp_kses_post( $total['value'] ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></td>
						</tr>
						<?php
				}
			?>
		</tfoot>
</table>

