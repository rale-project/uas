<?php

require_once __DIR__ . '/vendor/autoload.php';

add_action('init','the_pdf_quote_file');
function the_pdf_quote_file(){

	if( is_user_logged_in() &&  isset($_GET['pdfgenerator']) ){

		(int) $order_id = sanitize_text_field($_GET['pdfgenerator']);

		$date_pdf = current_time('m-d-Y');

		$mpdf = new \Mpdf\Mpdf();

		$mpdf->SetHTMLHeader( set_the_pdf_header(),2 );	

		$mpdf->WriteHTML( set_the_pdf_body($order_id,$date_pdf), 2 );

		$filename = 'Request-Quote-' . $order_id .'.pdf';

		// $mpdf->Output($filename, 'D');
		$mpdf->Output();
		
		#terminate mpdf script after the output this will fix the error in rendering
		#%PDF-1.4 %���� 3 0 obj <> /Annots [ 5 0 R ] /Contents 4 0 R>> endobj 4 0 obj <> stream
		exit();

	}

}

function inline_css_pdf(  ){
	
	$return = false;

	if( is_user_logged_in() &&  isset($_GET['pdfgenerator']) ){
		$return = true;
	}

	return $return;

}

function set_the_pdf_body( $order_id,$date_pdf ){

	ob_start();

	require_once "template/pdf-body.php";
	$content = ob_get_contents();

	ob_end_clean();

	return $content;

}

function set_the_pdf_header(){

	ob_start();

	require_once "template/pdf-header.php";
	$content = ob_get_contents();
	wp_reset_query();

	ob_end_clean();

	return $content;
}