<?php 

// Sets up the theme and adds some tweaks
require_once "includes/uas-setup.php";

// Registers any custom post types/taxonomies
require_once "includes/uas-cpt-tax.php";

// Enqueues theme scripts and stylesheets
require_once "includes/uas-frontend.php";

// Any WooCommerce filters/tweaks go here
require_once "includes/uas-woocommerce.php";

// Any filters/tweaks go here
require_once "includes/uas-theme-tweaks.php";

// admin tweaks dashboard
require_once "includes/uas-admin-tweaks.php";

// Generate Woo-Orders PDF Quotes
require_once "includes/pdf_quote/generate_pdf_quote.php";