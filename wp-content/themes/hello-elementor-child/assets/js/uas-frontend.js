jQuery(document).ready(function($){
    //Check if user is logged in then replace the header icon with its name
     var user_is_logged = false;
    if( typeof(getUserDetails) !== 'undefined' ){
        var userDetailsLoggedin = JSON.parse(getUserDetails);
        user_is_logged = true;
    }
    

    $('.product__page_listing li.product h2.woocommerce-loop-product__title, ul.products li.product h2.woocommerce-loop-product__title').matchHeight({ 
        byRow: true
   });

   $('.sku__and_cat').matchHeight({ 
        byRow: false
    });

   $('.product__page_listing ul.products li span.price').matchHeight({
        byRow: false
   });

   // CART PAGE 

   // if( $('body.woocommerce-cart').length ){

   //      if( localStorage.getItem("edit_order_id") ){
   //          var orderid_storage = localStorage.getItem("edit_order_id");
   //          if( $('div.woocommerce').length  ){
   //              if(  $('.edit_request_id_quote').length == 0 ){
   //                  $('div.woocommerce').prepend('<div class="edit_request_id_quote">Quote #' + orderid_storage + '</div>');
   //              }
                
   //          }
   //      }

   // }

   $(document).on('click','.pop-order-view-detail.edit_cart_items',function(e){

        e.preventDefault();

        var data = {
            'action'    : 'edit_items_to_cart',
            'order_id'  : $(this).attr('data-orderid')
        };
        var orderid = $(this).attr('data-orderid'); 

        $.ajax({
            url : ajaxurl,
            data : data,
            type : 'POST',
            dataType : 'JSON',
            beforeSend : function ( xhr ){
                // $('.processing-cart-data').text(' Saving Cart Items...');
            },
            success : function ( response ) {
                
                if ( response ){

                    if( typeof(Storage) !== 'undefined' ){
                        // check if cart is saved on local storage
                        localStorage.setItem("edit_order_id", orderid );

                    }else{
                        alert( 'Sorry, your browser does not support Web Storage.' );
                    }

                    window.location.replace(response.data.cart_url);                
                }

            }
        });

   });

   $(document).on('click','.pop-order-view-detail.view',function(e){

    e.preventDefault();

    var data = {

        'action' : 'load_view_order',
        'order_number' : $(this).attr('data-orderid')

    };

     $.ajax({
            url : ajaxurl,
            data : data,
            type : 'GET',
            dataType : 'html',
            beforeSend : function ( xhr ){
                // $('.processing-cart-data').text(' Saving Cart Items...');
            },
            success : function ( response ) {

                if( $('.custom-popup-order-view').length ){
                    $('.custom-popup-order-view').remove();
                }

                if ( response ){

                  $('.elementor-location-footer').append('<div class="custom-popup-order-view"></div>');
                  $.fancybox.open($('.custom-popup-order-view').html(response));
                  
                
                }

            }
        });

   });

   if( userDetailsLoggedin ){

        $('.user_check_loggedin ul.elementor-icon-list-items li:first-child span.elementor-icon-list-text').text( userDetailsLoggedin.user_nicename );
        $('.user_check_loggedin ul.elementor-icon-list-items li:first-child a').attr('href',userDetailsLoggedin.dashboard_link);
        
        if( $('.userloggedout-custom').length == 0 ) {
            $('.user_check_loggedin ').append('<div class="userloggedout-custom">not you? <a href="#" class="ctm-logout-header">logout</a></div>');
        }
	   
	   $(document).on('click','.ctm-logout-header', function(e){
		   e.preventDefault();
		  	$('.hidden-logout-footer-link')[0].click();
	   });
	   
        $('.hide_whenloggedin').css('display','none');

        if( userDetailsLoggedin.user_role == 'customer' ){
            $('.quote_details_sidebar').remove();
        }
   

    }

        
    if( user_is_logged == false ){
        if( $('body.woocommerce-cart').length ){
            $('.cart__wrapper').append('<div class="block-user-not-loggedin"><p> You must logged in to be able to send request. <a href="#" class="trigger-popupuserloggedin">Click Here</a></p></div>');
    
            $(document).on('click','.trigger-popupuserloggedin',function(e){
                e.preventDefault();
                $('.user_check_loggedin ul.elementor-icon-list-items li:first-child a').click();        
            });
        }

    }

    function save_cart_items_as_request_quote(){

        // below  this this will update the order ITEM based on order id
        if( localStorage.getItem("edit_order_id") ){

            // work on this
            $('.savingcart-items').remove();

            var data = {
                'action'        : 'save_edited_cart',
                'order_number'  : localStorage.getItem("edit_order_id")
            }

            $.ajax({
                url : ajaxurl,
                data : data,
                type : 'post',
                dataType : 'JSON',
                beforeSend : function ( xhr ){
                    $('.wc-proceed-to-checkout').append('<p class="savingcart-items">Updating Quote Items...</p>');
                },
                success : function ( data ) {

                    if ( data ){
                        localStorage.setItem("edit_order_id", '');
                        localStorage.setItem("saved_cart_order", '');
                        
                        $('.savingcart-items').text('');
                        
                        var dashboard_url = data.data.dashboard_link+'?orderid='+data.data.ordernumber;

                        window.location.replace(dashboard_url);
                        
                    }

                }
            });

        }

        // below this it will save the cart to order
        if( localStorage.getItem("saved_cart_order") == '' && ! localStorage.getItem("get_order_id")  || localStorage.getItem("saved_cart_order") == null && ! localStorage.getItem("get_order_id") ){

            $('.savingcart-items').remove();

            if( typeof(Storage) !== 'undefined' ){
                // check if cart is saved on local storage
                if( localStorage.getItem('saved_cart_order') ){
                    data = {
                        'action' : 'save_item_cart',
                        'order_number' : localStorage.getItem('saved_cart_order'),
                        'current_user' : userDetailsLoggedin.user_role
                    };
                }else{
                    data = {
                        'action' : 'save_item_cart',
                        'current_user' : userDetailsLoggedin.user_role
                    };
                }

            }else{
                alert( 'Sorry, your browser does not support Web Storage.' );
            }

            $.ajax({
                url : ajaxurl,
                data : data,
                type : 'post',
                dataType : 'JSON',
                beforeSend : function ( xhr ){
                    $('.wc-proceed-to-checkout').append('<p class="savingcart-items">Saving Cart Items...</p>');
                },
                success : function ( data ) {

                    if ( data ){
                        localStorage.setItem("saved_cart_order", data.data.ordernumber);
                        $('#form-field-field_9').val(data.data.ordernumber);
                        $('#form-field-field_10').val(data.data.orderlink);
                        localStorage.setItem("saved_cart_order", '');
                        $('.savingcart-items').text('');
                        
                        if( userDetailsLoggedin.user_role == 'customer' ){
                            $('.savingcart-items').append('<a href="'+data.data.dashboard_link+'?orderid='+data.data.ordernumber+'" class="view-dashboard-quote">#'+data.data.ordernumber+'</a> Thanks for placing your order with United Automotive Solutions, customer service will be in touch with you to finalise your order.');
                        }

                        if( userDetailsLoggedin.user_role == 'whole_saler' ){
                            $('.savingcart-items').append('<a href="'+data.data.dashboard_link+'?orderid='+data.data.ordernumber+'" class="view-dashboard-quote">#'+data.data.ordernumber+'</a> Thanks for placing a quote with United Automotive Solutions. You can download this quote as a PDF on your dashboard.');
                        }
                        
                    }

                }
            });    

        }

    }

    $(document).on('change','#form-field-email_request_cart_quote',function(e){

        if( user_is_logged == false ){
            
            return false;
        }

        save_cart_items_as_request_quote();


    });


    $(document).on('click','.wc-proceed-to-checkout .checkout-button',function(e){

        // alert( localStorage.getItem("saved_cart_order") );

        if( userDetailsLoggedin.user_role !== 'customer' ) {
            e.preventDefault();
            save_cart_items_as_request_quote();
        }


    });

    $(document).on('change','.woocommerce-cart-form__contents .product-quantity input.input-text.qty',function(e){
        setTimeout(function(){ 
            $('.woocommerce-cart-form__contents tr .actions button')[0].click();
             }, 500);
    });


    var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;
 
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
     
            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

    var url_para_orderid = getUrlParameter('orderid');

    if( url_para_orderid !== 'undefined' ){
        
        $('.pop-order-view-detail.view').each(function(){
            var orderid_pop = $(this).attr('data-orderid');
            if( parseInt(orderid_pop) === parseInt(url_para_orderid) ){
                $(this).click();
            }    
        
        });

    }

    $('body.elementor-page-470 #form-field-field_11,body.elementor-page-451 #form-field-field_7').on('keyup', function() {
        limitText(this, 8)
    });


    $('body.elementor-page-470 .elementor-field-group-field_9').append('<i aria-hidden="true" id="showpassword" class="fas fa-eye"></i>');
    $('body.elementor-page-451 .elementor-field-group-field_10').append('<i aria-hidden="true" id="showpassword" class="fas fa-eye"></i>');


    $(document).on('click', '#showpassword', function() {

    	var $show_password = $(this).parent().find('input')[0].id;

    	if( $('#'+ $show_password).attr('type') == 'password' ) {
    		$('#'+ $show_password).attr('type','text');
    	}else{
    		$('#'+ $show_password).attr('type','password');
    	}
    });

    

    function limitText(field, maxChar){
        var ref = $(field),
            val = ref.val();
        if ( val.length >= maxChar ){
            ref.val(function() {
                // console.log(val.substr(0, maxChar))
                return val.substr(0, maxChar);       
            });
        }
    }

    $('.order-actions a.download_quote').each(function(){
        $(this).attr('target','_blank');
    });

});