<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 *
 * @see woocommerce_default_product_tabs()
 */
$product_tabs = apply_filters( 'woocommerce_product_tabs', array() );

global $product;

$add_info_tab 			= get_field('content_addinfo',$product->ID);
$waranty_tab 			= get_field('file_warranty',$product->ID);
$core_return_tab		= get_field('core_returns',$product->ID);

$custom_product_tabs 	= array(

	'additional-info' => array(
		'title' => 'Additional Information',
		'callback' 		=>	$add_info_tab,
		'file'	=> 0,
	),
	'file-warranty'	  => array(
		'title' => 'Warranty',
		'callback' 		=>	$waranty_tab,
		'file'	=> 1,
	),
	'core-return'	  => array(
		'title' 		=> 	'Core Returns',
		'callback' 		=>	$core_return_tab,
		'file'	=> 0,
	),
);

if ( ! empty( $custom_product_tabs ) ) : ?>

	<div class="woocommerce-tabs wc-tabs-wrapper">
		<ul class="tabs wc-tabs" role="tablist">
			<?php foreach ( $custom_product_tabs as $key => $product_tab ) : ?>
				<li class="<?php echo esc_attr( $key ); ?>_tab" id="tab-title-<?php echo esc_attr( $key ); ?>" role="tab" aria-controls="tab-<?php echo esc_attr( $key ); ?>">
					<a href="#tab-<?php echo esc_attr( $key ); ?>">
						<?php echo wp_kses_post( apply_filters( 'woocommerce_product_' . $key . '_tab_title', $product_tab['title'], $key ) ); ?>
					</a>
				</li>
			<?php endforeach; ?>
		</ul>


		<?php foreach ( $custom_product_tabs as $key => $product_tab ) : ?>
			<div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--<?php echo esc_attr( $key ); ?> panel entry-content wc-tab" id="tab-<?php echo esc_attr( $key ); ?>" role="tabpanel" aria-labelledby="tab-title-<?php echo esc_attr( $key ); ?>">
				<?php
				if ( $product_tab['callback']  && $product_tab['file'] == 0 ) {
					echo apply_filters('the_content',$product_tab['callback']);
				}else{
					?>
						<a href="<?php echo $product_tab['callback']['url']; ?>">
							<?php echo $product_tab['callback']['title']; ?>
						</a>
					<?php
				}
				?>
			</div>
		<?php endforeach; ?>

		<?php do_action( 'woocommerce_product_after_tabs' ); ?>
	</div>

<?php endif; ?>
