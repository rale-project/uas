<?php
/**
 * My Orders - Deprecated
 *
 * @deprecated 2.6.0 this template file is no longer used. My Account shortcode uses orders.php.
 * @package WooCommerce/Templates
 */

defined( 'ABSPATH' ) || exit;

$my_orders_columns = apply_filters(
	'woocommerce_my_account_my_orders_columns',
	array(
		'order-number'  => esc_html__( 'Order', 'woocommerce' ),
		'order-date'    => esc_html__( 'Date', 'woocommerce' ),
		'order-status'  => esc_html__( 'Status', 'woocommerce' ),
		'order-total'   => esc_html__( 'Total', 'woocommerce' ),
		'date-expiry'	=> esc_html__( 'Date Expiry', 'woocommerce' ),
		'order-actions' => '&nbsp;',
	)
);

// wc_get_order_statuses()

// Custom order status query

$current_user = wp_get_current_user();


if( is_page(715) ){

	$page_title = 'Orders';

	if( $current_user->roles[0] == 'whole_saler' ){
		$order_statuses = array( 
		        'wc-pending' => _x( 'Pending payment', 'Order status', 'woocommerce' ),  
		        'wc-processing' => _x( 'Processing', 'Order status', 'woocommerce' ),  
		        'wc-on-hold' => _x( 'On hold', 'Order status', 'woocommerce' ),  
		        'wc-completed' => _x( 'Completed', 'Order status', 'woocommerce' ),  
		        'wc-cancelled' => _x( 'Cancelled', 'Order status', 'woocommerce' ),  
		        'wc-refunded' => _x( 'Refunded', 'Order status', 'woocommerce' ),  
		        'wc-failed' => _x( 'Failed', 'Order status', 'woocommerce' ),  
		 );	
	}else{
		$order_statuses = array(
				'wc-request-quote' => _x( 'Request a Quote', 'Order status', 'woocommerce' ),   
		        'wc-pending' => _x( 'Pending payment', 'Order status', 'woocommerce' ),  
		        'wc-processing' => _x( 'Processing', 'Order status', 'woocommerce' ),  
		        'wc-on-hold' => _x( 'On hold', 'Order status', 'woocommerce' ),  
		        'wc-completed' => _x( 'Completed', 'Order status', 'woocommerce' ),  
		        'wc-cancelled' => _x( 'Cancelled', 'Order status', 'woocommerce' ),  
		        'wc-refunded' => _x( 'Refunded', 'Order status', 'woocommerce' ),  
		        'wc-failed' => _x( 'Failed', 'Order status', 'woocommerce' ),  
		 );
	}
	

}
if( is_page(734) ){

	$page_title = 'Quotes';

	if( $current_user->roles[0] == 'whole_saler' ){
		$order_statuses = array( 
	    	'wc-request-quote' => _x( 'Request a Quote', 'Order status', 'woocommerce' ),  
		 );
	}else{
		$order_statuses = array(
				'wc-request-quote' => _x( 'Request a Quote', 'Order status', 'woocommerce' ),   
		        'wc-pending' => _x( 'Pending payment', 'Order status', 'woocommerce' ),  
		        'wc-processing' => _x( 'Processing', 'Order status', 'woocommerce' ),  
		        'wc-on-hold' => _x( 'On hold', 'Order status', 'woocommerce' ),  
		        'wc-completed' => _x( 'Completed', 'Order status', 'woocommerce' ),  
		        'wc-cancelled' => _x( 'Cancelled', 'Order status', 'woocommerce' ),  
		        'wc-refunded' => _x( 'Refunded', 'Order status', 'woocommerce' ),  
		        'wc-failed' => _x( 'Failed', 'Order status', 'woocommerce' ),  
		 );
	}
}


$customer_orders = get_posts(
	apply_filters(
		'woocommerce_my_account_my_orders_query',
		array(
			'numberposts' => $order_count,
			'meta_key'    => '_customer_user',
			'meta_value'  => get_current_user_id(),
			'post_type'   => wc_get_order_types( 'view-orders' ),
			'post_status' => array_keys( $order_statuses  ),
		)
	)
);

if ( $customer_orders ) : ?>

	<h2 class="ctm-order-title"><?php echo apply_filters( 'woocommerce_my_account_my_orders_title', esc_html__( $page_title, 'woocommerce' ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></h2>

	<table class="shop_table shop_table_responsive my_account_orders">

		<thead>
			<tr>
				<?php foreach ( $my_orders_columns as $column_id => $column_name ) : ?>
					<th class="<?php echo esc_attr( $column_id ); ?>"><span class="nobr"><?php echo esc_html( $column_name ); ?></span></th>
				<?php endforeach; ?>
			</tr>
		</thead>

		<tbody>
			<?php
			foreach ( $customer_orders as $customer_order ) :
				$order      = wc_get_order( $customer_order ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited
				$item_count = $order->get_item_count();
				?>
				<tr class="order">
					<?php foreach ( $my_orders_columns as $column_id => $column_name ) : ?>
						<td class="<?php echo esc_attr( $column_id ); ?>" data-title="<?php echo esc_attr( $column_name ); ?>">
							<?php if ( has_action( 'woocommerce_my_account_my_orders_column_' . $column_id ) ) : ?>
								<?php do_action( 'woocommerce_my_account_my_orders_column_' . $column_id, $order ); ?>

							<?php elseif ( 'order-number' === $column_id ) : ?>
								<a href="<?php //echo esc_url( $order->get_view_order_url() ); ?>" onclick="return false;">
									<?php echo _x( '#', 'hash before order number', 'woocommerce' ) . $order->get_order_number(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
								</a>

							<?php elseif ( 'order-date' === $column_id ) : ?>
								<time datetime="<?php echo esc_attr( $order->get_date_created()->date( 'c' ) ); ?>"><?php echo esc_html( wc_format_datetime( $order->get_date_created() ) ); ?></time>

							<?php elseif ( 'order-status' === $column_id ) : ?>
								<?php echo esc_html( wc_get_order_status_name( $order->get_status() ) ); ?>

							<?php elseif ( 'order-total' === $column_id ) : ?>
								<?php
								/* translators: 1: formatted order total 2: total order items */
								printf( _n( '%1$s for %2$s item', '%1$s for %2$s items', $item_count, 'woocommerce' ), $order->get_formatted_order_total(), $item_count ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
								?>

							<?php elseif ( 'order-actions' === $column_id ) : ?>
								<?php
								$actions = wc_get_account_orders_actions( $order );
								
								if ( ! empty( $actions ) ) {
									foreach ( $actions as $key => $action ) { // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited
										echo '<a href="' . esc_url( $action['url'] ) . '" data-orderid="'. $order->get_order_number() .'" class="pop-order-view-detail button ' . sanitize_html_class( $key ) . '">' . esc_html( $action['name'] ) . '</a>';
									}
								}
								?>
							<?php elseif ( 'date-expiry' === $column_id ) : ?>
								<?php 
									$date_expiration = get_field( 'date_expiry',$order->get_order_number() );

									if( $date_expiration ){
										echo get_field( 'date_expiry', $order->get_order_number() );
									}

								?>
							<?php endif; ?>


						</td>
					<?php endforeach; ?>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<?php else:  ?>
		<p style="font-style:italic;"><?php echo apply_filters( 'woocommerce_my_account_my_orders_title', esc_html__( 'No Entries Found', 'woocommerce' ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>

<?php endif; ?>
