<?php
if (!defined( 'ABSPATH' ) ) { exit;}
if( !class_exists( 'ni_custom_order_status_init' ) ) {
	class ni_custom_order_status_init {
		
		var $vars =  array(); 
		public function __construct(){
			
			add_action( 'admin_menu',  array(&$this,'admin_menu' ));
			add_action( 'admin_init',  array( &$this, 'admin_init' ));
			
			/*Register Status*/
			add_filter( 'init',  array(&$this,'init' ));
			
			/*Add Status Order*/
			add_filter( 'wc_order_statuses',  array(&$this,'ni_wc_add_order_statuses') );
			
			
			/*3rd Save Post*/
			add_action( 'save_post', array( &$this, 'save_post'), 10, 2 );
			
			add_action( 'admin_enqueue_scripts',  array(&$this,'admin_enqueue_scripts' ));
			
			/*For Order Status Color and Circle*/
			add_action('admin_head', array(&$this,'admin_head'));
		
			/*Add Custom Columns into list page*/	
			add_filter( 'manage_edit-ni-order-status_columns',  array( &$this,'order_status_list_columns' ));
			
			/*Add Value to Columns*/
			add_action( 'manage_posts_custom_column', array( &$this,'order_status_list_columns_value' ), 10, 2 );
			
			/*Add Next Action*/
			add_action( 'woocommerce_admin_order_actions', array( &$this,'ni_next_order_actions' ), 10, 2 );
			
		}
		function admin_enqueue_scripts(){
			global $typenow;
			if(  $typenow == 'ni-order-status' ) {
				wp_enqueue_style( 'wp-color-picker' );
				//wp_enqueue_script( 'ni-custom-order-status-color-js', plugins_url( '../js/color.js', __FILE__ ), array( 'wp-color-picker' ) );
				wp_enqueue_script( 'ajax-script-ni-custom-color', plugins_url( '../js/ni-custom-color-script.js', __FILE__ ), array('wp-color-picker' ) );
				
				//wp_register_style('ni-custom-order-status-style', plugins_url( '../css/ni-custom-order-status-style.css', __FILE__ ));
				//wp_enqueue_style('ni-custom-order-status-style' );
			}
			
			 if (isset($_REQUEST["page"])){
		     	$page = $_REQUEST["page"];
				if ($page == "ni-custom-order-status-report" || $page == "niwoocos-other-plugins"){
					wp_register_style( 'ni-sales-report-summary-css', plugins_url( '../assets/css/ni-sales-report-summary.css', __FILE__ ));
		 			wp_enqueue_style( 'ni-sales-report-summary-css' );
					wp_register_style( 'ni-font-awesome-css', plugins_url( '../assets/css/font-awesome.css', __FILE__ ));
		 			wp_enqueue_style( 'ni-font-awesome-css' );
					
					wp_register_script( 'ni-amcharts-script', plugins_url( '../assets/js/amcharts/amcharts.js', __FILE__ ) );
					wp_enqueue_script('ni-amcharts-script');
				
		
					wp_register_script( 'ni-light-script', plugins_url( '../assets/js/amcharts/light.js', __FILE__ ) );
					wp_enqueue_script('ni-light-script');
				
					wp_register_script( 'ni-pie-script', plugins_url( '../assets/js/amcharts/pie.js', __FILE__ ) );
					wp_enqueue_script('ni-pie-script');
					
					
					wp_register_style('nicos-bootstrap-css', plugins_url('../assets/css/lib/bootstrap.min.css', __FILE__ ));
		 			wp_enqueue_style('nicos-bootstrap-css' );
				
					wp_enqueue_script('nicos-bootstrap-script', plugins_url( '../assets/js/lib/bootstrap.min.js', __FILE__ ));
					wp_enqueue_script('nicos-popper-script', plugins_url( '../assets/js/lib/popper.min.js', __FILE__ ));
					
				}
			 }
			
		}
		function admin_menu(){
			add_menu_page('Order Status', 'Order Status', 'manage_options', 'ni-custom-order-status',  array(&$this,'add_page'),  plugins_url( '../images/icon.png', __FILE__ ),14);
			add_submenu_page('ni-custom-order-status', 'Report', 'Report', 'manage_options', 'ni-custom-order-status-report' , array(&$this,'add_page'));
			
				add_submenu_page('ni-custom-order-status', 'Other Plugins', 'Other Plugins', 'manage_options', 'niwoocos-other-plugins' , array(&$this,'add_page'));
		}
		function admin_head(){
			global $typenow;
			//$page = $_REQUEST[""] 
			if( $typenow == 'ni-order-status' || $typenow == 'shop_order' ) {
				 /*Hide Permalink*/
				 echo "<style>#edit-slug-box {display:none;}</style>";	 
				$custom_order_status = $this->get_custom_post_type_order_status();
				
				foreach ($custom_order_status as $k => $v) { 
					$color =	$v["ni_order_status_color"];
					$slug =	$v["ni_order_status_slug"];
					echo '<style>
					mark.status-'.$slug.'{
							background-color:'. $color .';
							color:#FFFFFF;
					}
					</style>';
					
					/*echo '<style>
					.widefat .column-order_status mark.'.$slug .':after{
						font-family:WooCommerce;
						speak:none;
						font-weight:400;
						font-variant:normal;
						text-transform:none;
						line-height:1;
						-webkit-font-smoothing:antialiased;
						margin:0;
						text-indent:0;
						position:absolute;
						top:0;
						left:0;
						width:100%;
						height:100%;
						text-align:center;
					}
					.widefat .column-order_status mark.'.$slug .':after{
						background-color:'. $color .';
						content:"\e039";
						color:'. $color .';
						border-radius:50%;
						
					}
					</style>';*/
				}
			}
		}
		function add_page(){
			//$page = isset($_REQUEST["page"])?$_REQUEST["page"] :''
			if ( isset($_REQUEST["page"])){
				$page = $_REQUEST["page"];
				if ($page =="ni-custom-order-status-report"){
					include_once("ni-custom-order-status-report.php");
					$obj = new ni_custom_order_status_report();
					$obj->page_init();
				}
				if ($page=="niwoocos-other-plugins")
				{
					include_once("ni-addons-order-custom.php");
					$obj =  new ni_addons_order_custom();
					$obj->page_init();
					
				}
			}
			//echo "To Do..........!";	
			
		}
		function admin_init(){
			$this->add_meta_box();
		}
		function ni_wc_register_post_statuses(){
			
			$custom_order_status = $this->get_custom_post_type_order_status();
			
			/*Loop For Custom Order Status*/
			foreach ($custom_order_status as $k => $v) { 
			
					$id = preg_replace('#[ -]+#', '-', $v["ni_order_status_slug"]);
					$id = "wc-".strtolower($id);
					$label =  $v["ni_order_status_title"];
				
				register_post_status( trim($id), array(
					'label'                     => _x( $label, 'WooCommerce Order status', 'ni_custom_order_status' ),
					'public'                    => true,
					'exclude_from_search'       => false,
					'show_in_admin_all_list'    => true,
					'show_in_admin_status_list' => true,
					'label_count'               => _n_noop( $label.' <span class="count">(%s)</span>', $label.'<span class="count">(%s)</span>' )
				) );
			}
		}
		function ni_wc_add_order_statuses( $order_statuses ) {
			
			$custom_order_status = $this->get_custom_post_type_order_status();
			
			foreach ($custom_order_status as $k => $v) { 
				$id = preg_replace('#[ -]+#', '-', $v["ni_order_status_slug"]);
				$id = "wc-".strtolower($id);
				$label =  $v["ni_order_status_title"];
				
				$order_statuses[trim($id)] = _x( $label, 'WooCommerce Order status', 'ni_custom_order_status' );
			}
			//echo '<pre>',print_r($order_statuses,1),'</pre>';	
			return $order_statuses;
		}
		function init(){
			$this->ni_register_order_status_post_type();
			$this->ni_wc_register_post_statuses();
		}
		function ni_register_order_status_post_type(){
		
			register_post_type( 'ni-order-status', /*Name of Custome Post Type */
					array(
						'labels' => array(
							'name' => 'Order Status',
							'singular_name' => 'Order Status',
							'add_new' => 'Add New',
							'add_new_item' => 'Add New Order Status',
							'edit' => 'Edit',
							'edit_item' => 'Edit Order Status',
							'new_item' => 'New Order Status',
							'view' => 'View',
							'view_item' => 'View Order Status',
							'search_items' => 'Search Order Status',
							'not_found' => 'No order status found',
							'not_found_in_trash' => 'No Order Status found in Trash',
							'parent' => 'Parent Order Status',
						),
						'public' => true,
						'show_in_menu' => 'ni-custom-order-status',
						'menu_position' => 15,
						'supports' => array( 'title'),
						'taxonomies' => array( '' ),
						'menu_icon' => plugins_url( 'images/image.png', __FILE__ ),
						'has_archive' => true,
						
					)
				);
		}
		/*display Meta Box*/
		function ni_display_custom_order_status_meta_box($order_status)
		{
			// Retrieve current slug and color base on the post id
			$order_status_slug = esc_html( get_post_meta( $order_status->ID, '_ni_order_status_slug', true ) );
			
			$order_status_color = esc_html( get_post_meta( $order_status->ID, '_ni_order_status_color', true ) );
			
			$order_status_description = esc_html( get_post_meta( $order_status->ID, '_ni_order_status_description', true ) );
			//#ffffff
			?>
			<table style="border:1px solid #FFFFFF; width:50%">
				<tr>
					<td style="width: 100%"> <label for="_ni_order_status_slug" class="ni-order-status-slug"><?php _e( 'Order Status Slug', 'ni_custom_order_status' )?></label></td>
					<td><input type="text" size="60" name="_ni_order_status_slug" id="_ni_order_status_slug" value="<?php echo $order_status_slug; ?>" maxlength="18"  /></td>
				</tr>
				<tr>
					<td style="width: 100%"> <label for="_ni_order_status_color" class="prfx-row-title"><?php _e( 'Color Picker', 'ni_custom_order_status' )?></label></td>
					<td><input name="_ni_order_status_color" type="text" value="<?php echo isset($order_status_color)?$order_status_color: "#ffffff"; ?>"  id="_ni_order_status_color" class="order_status_color" /></td>
				</tr>
                <tr>
					<td style="width: 100%"> <label for="_ni_order_status_description" class="ni-order-status-description"><?php _e( 'Description', 'ni_custom_order_status' )?></label></td>
					<td>
                    <textarea name="_ni_order_status_description" id="_ni_order_status_description" rows="6" cols="30"><?php echo $order_status_description; ?> </textarea>
                    </td>
				</tr>
			</table>
			<?php
		}
		function get_custom_post_type_order_status(){
			
			global $wpdb;	
			$custom_order_status =  array();
			if(!isset($this->vars['custom_order_status'])){
				
				
				$query = "SELECT
						posts.post_title
						,slug.meta_value as ni_order_status_slug
						,color.meta_value as ni_order_status_color
						FROM {$wpdb->prefix}posts as posts		
					
						LEFT JOIN  {$wpdb->prefix}postmeta as slug ON slug.post_id=posts.ID 
						LEFT JOIN  {$wpdb->prefix}postmeta as color ON color.post_id=posts.ID 	
						
						WHERE 
								posts.post_type ='ni-order-status' 
								AND posts.post_status ='publish'
								AND slug.meta_key='_ni_order_status_slug'
								AND color.meta_key='_ni_order_status_color'
								
						";
					$results = $wpdb->get_results( $query);	
					
					//echo mysql_error();
					
				//echo '<pre>',print_r($results,1),'</pre>';	
				foreach ($results as $k => $v) { 
					$custom_order_status[$k]["ni_order_status_title"] = $v->post_title;
					$custom_order_status[$k]["ni_order_status_slug"]  = $v->ni_order_status_slug;
					$custom_order_status[$k]["ni_order_status_color"] = isset($v->ni_order_status_color)?$v->ni_order_status_color :  "#ffffff";
					
					
				}
				$this->vars['custom_order_status'] = $custom_order_status;
			}else{
				$custom_order_status = $this->vars['custom_order_status'];
			}
			
			return $custom_order_status;
		}
		function add_meta_box(){
			add_meta_box( 'custom_order_status_meta_box', 
					'Custom Order Status Meta Box',
					array( &$this, 'ni_display_custom_order_status_meta_box'), /*Name of Call Back or Display Meta Box Function*/
					'ni-order-status', /*Custom Post Type Name*/
					'normal', 
					'high'
				);
		}
		
		function save_post($status_id, $status)
		{
			//echo $movie_review;
			//die;
			
			if ( $status->post_type == 'ni-order-status' ) {
				// Store data in post meta table if present in post data
				if ( isset( $_POST['_ni_order_status_slug'] ) && $_POST['_ni_order_status_slug'] != '' ) {
					update_post_meta( $status_id, '_ni_order_status_slug', $_POST['_ni_order_status_slug'] );
				}
				if ( isset( $_POST['_ni_order_status_color'] ) && $_POST['_ni_order_status_color'] != '' ) {
					update_post_meta( $status_id, '_ni_order_status_color', $_POST['_ni_order_status_color'] );
				}else{
					update_post_meta( $status_id, '_ni_order_status_color', "#ffffff" );
				}
				if ( isset( $_POST['_ni_order_status_description'] ) && $_POST['_ni_order_status_description'] != '' ) {
					update_post_meta( $status_id, '_ni_order_status_description', $_POST['_ni_order_status_description'] );
				}
			}
		}
		/*Columns List*/
		function order_status_list_columns($columns){
		
				unset( $columns['date'] );
				$columns['_ni_order_status_slug'] = 'Slug';
				$columns['_ni_order_status_color'] = 'Color';
				$columns['_ni_order_status_description'] = 'Description';
				$columns['date'] = 'Date';
			   
				return $columns;	
		}
		function order_status_list_columns_value($column,$post_id){
			 if ( '_ni_order_status_slug' == $column ) {
					$ni_order_status_slug = esc_html( get_post_meta( get_the_ID(), '_ni_order_status_slug', true ) );
					echo $ni_order_status_slug;
			 }
			 if ( '_ni_order_status_description' == $column ) {
					$ni_order_status_description = esc_html( get_post_meta( get_the_ID(), '_ni_order_status_description', true ) );
					echo $ni_order_status_description;
			 }
			 if ( '_ni_order_status_color' == $column ) {
					$ni_order_status_color = esc_html( get_post_meta( get_the_ID(), '_ni_order_status_color', true ) );
					echo "<div style=\"height:25px; width:25px; background-color:". $ni_order_status_color."\"></div>";
			 }
		}
		function ni_next_order_actions($actions , $the_order ){
				//$actions = array();
			global $post;
			$custom_order_status_string = "";
			$custom_order_status  = $this->get_custom_post_type_order_status();
		
			 
			$custom_array = array(); 
			foreach($custom_order_status  as $k =>$v){
			
				$custom_array[] =  $v["ni_order_status_slug"];
			}
			if ( $the_order->has_status( $custom_array) ) {
				$actions['complete'] = array(
					'url'       => wp_nonce_url( admin_url( 'admin-ajax.php?action=woocommerce_mark_order_status&status=completed&order_id=' . $post->ID ), 'woocommerce-mark-order-status' ),
					'name'      => __( 'Complete', 'woocommerce' ),
					'action'    => "complete"
				);
			}
			return $actions ;
		}
		
	}
}
?>