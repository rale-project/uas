<?php
/*
Plugin Name: Product Category Slider for Elementor
Plugin URI: 
Description: WooCommerce Category Slider elementor helps you display WooCommerce Categories aesthetically in a nice sliding manner. You can manage and show your product categories with thumbnail, child category (beside), description, shop now button with an easy to use shortcode generator interface with many handy options.
Version: 1.0.3
Author: theimran
Author URI: http://www.theimran.com/
Copyright: theimran
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html
Text Domain: product-category-slider-for-elementor
*/
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
    die;
}

define('PCSFE_PLUGIN_PATH', plugin_dir_path( __file__ ));
define('PCSFE_TEXT_DOMAIN', 'product-category-slider-for-elementor');
require_once PCSFE_PLUGIN_PATH . 'pcsfe_elementor.php';


/**
 * Product Category Slider Main Class
 */
class Product_Category_Slider_Main
{
	
	function __construct()
	{
		add_action( 'wp_enqueue_scripts', array($this, 'pcsfe_slider_scripts' ), 99 );
		add_action( 'admin_enqueue_scripts', array($this, 'pcsfe_admin_scripts') );
		$plugin = plugin_basename( __FILE__ );
		add_filter( "plugin_action_links_$plugin", array($this, 'plugin_add_settings_link') );
		
	}
	
	public function pcsfe_slider_scripts() {
		wp_enqueue_style( 'owl-carousel', plugin_dir_url(__file__) . 'asset/css/owl.carousel.css' );
		wp_enqueue_style( 'pcsfe-style', plugin_dir_url(__file__) . 'asset/css/style.css' );
		wp_enqueue_script( 'owl-carousel', plugin_dir_url(__file__) . 'asset/js/owl.carousel.js', array( 'jquery' ), null, true );
		wp_enqueue_script( 'pcsfe-main', plugin_dir_url(__file__) . 'asset/js/main.js', array( 'jquery' ), null, true );
	}

	public function pcsfe_admin_scripts(){
		wp_enqueue_style( 'pcsfe-admin-style', plugin_dir_url(__file__) . 'asset/admin/style.css' );
	}

	public function plugin_add_settings_link( $links ) {
	    $settings_link = '<a style="color: red; font-weight: 700;" href="#">Go To Pro</a>';
	    array_push( $links, $settings_link );
	    return $links;
	}


}

$init_product_category_slider_main = new Product_Category_Slider_Main;